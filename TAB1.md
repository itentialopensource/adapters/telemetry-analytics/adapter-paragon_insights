# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Paragon_insights System. The API that was used to build the adapter for Paragon_insights is usually available in the report directory of this adapter. The adapter utilizes the Paragon_insights API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Paragon Insights adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon Insights. With this adapter you have the ability to perform operations such as:

- Get real-time monitoring of network performance metrics.
- Get details about network logs, health status, and outliers.
- Gather and analyze configuration and telemetry data from network devices and infrastructure components.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
