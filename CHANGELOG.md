
## 0.5.4 [10-14-2024]

* Changes made at 2024.10.14_19:39PM

See merge request itentialopensource/adapters/adapter-paragon_insights!15

---

## 0.5.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-paragon_insights!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_17:43PM

See merge request itentialopensource/adapters/adapter-paragon_insights!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_18:39PM

See merge request itentialopensource/adapters/adapter-paragon_insights!11

---

## 0.5.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!10

---

## 0.4.0 [05-01-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!9

---

## 0.3.0 [05-01-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!9

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:08PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!8

---

## 0.2.3 [03-21-2024]

* Changes made at 2024.03.21_14:56PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!7

---

## 0.2.2 [03-12-2024]

* Changes made at 2024.03.12_10:55AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!6

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:28AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!5

---

## 0.2.0 [01-04-2024]

* Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!4

---

## 0.1.3 [08-11-2023]

* Add calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!2

---

## 0.1.2 [05-01-2023]

* Add multi step authentication

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!1

---

## 0.1.1 [03-13-2023]

* Bug fixes and performance improvements

See commit 63a1ffe

---
