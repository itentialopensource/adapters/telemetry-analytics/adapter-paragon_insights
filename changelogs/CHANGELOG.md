
## 0.1.2 [05-01-2023]

* Add multi step authentication

See merge request itentialopensource/adapters/telemetry-analytics/adapter-paragon_insights!1

---

## 0.1.1 [03-13-2023]

* Bug fixes and performance improvements

See commit 63a1ffe

---
