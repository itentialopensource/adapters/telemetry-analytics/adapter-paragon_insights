/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-paragon_insights',
      type: 'ParagonInsights',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const ParagonInsights = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Paragon_insights Adapter Test', () => {
  describe('ParagonInsights Class Tests', () => {
    const a = new ParagonInsights(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('paragon_insights'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('paragon_insights'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('ParagonInsights', pronghornDotJson.export);
          assert.equal('Paragon_insights', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-paragon_insights', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('paragon_insights'));
          assert.equal('ParagonInsights', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-paragon_insights', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-paragon_insights', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#retrieveOrchestrator - errors', () => {
      it('should have a retrieveOrchestrator function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveOrchestrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rollbackUnsavedConfiguration - errors', () => {
      it('should have a rollbackUnsavedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.rollbackUnsavedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAffectedGroups - errors', () => {
      it('should have a retrieveAffectedGroups function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAffectedGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#commitUnsavedConfiguration - errors', () => {
      it('should have a commitUnsavedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.commitUnsavedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#initialize - errors', () => {
      it('should have a initialize function', (done) => {
        try {
          assert.equal(true, typeof a.initialize === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestByoiIngestMappings - errors', () => {
      it('should have a deleteHealthbotIngestByoiIngestMappings function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestByoiIngestMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsByoiIngestMappings - errors', () => {
      it('should have a deleteHealthbotIngestSettingsByoiIngestMappings function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsByoiIngestMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkDeviceGroupUnsavedConfiguration - errors', () => {
      it('should have a checkDeviceGroupUnsavedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.checkDeviceGroupUnsavedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.checkDeviceGroupUnsavedConfiguration(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-checkDeviceGroupUnsavedConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkNetworkGroupUnsavedConfiguration - errors', () => {
      it('should have a checkNetworkGroupUnsavedConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.checkNetworkGroupUnsavedConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.checkNetworkGroupUnsavedConfiguration(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-checkNetworkGroupUnsavedConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceGroupDeviceGroup - errors', () => {
      it('should have a retrieveIcebergDeviceGroupDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceGroupDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDeviceGroupDeviceGroupById - errors', () => {
      it('should have a deleteIcebergDeviceGroupDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDeviceGroupDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.deleteIcebergDeviceGroupDeviceGroupById(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceGroupDeviceGroupById - errors', () => {
      it('should have a retrieveIcebergDeviceGroupDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceGroupDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveIcebergDeviceGroupDeviceGroupById(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergDeviceGroupDeviceGroupById - errors', () => {
      it('should have a createIcebergDeviceGroupDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergDeviceGroupDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.createIcebergDeviceGroupDeviceGroupById(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergDeviceGroupDeviceGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergDeviceGroupDeviceGroupById - errors', () => {
      it('should have a updateIcebergDeviceGroupDeviceGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergDeviceGroupDeviceGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.updateIcebergDeviceGroupDeviceGroupById(null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergDeviceGroupDeviceGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDeviceGroupDeviceGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDeviceGroupStatus - errors', () => {
      it('should have a retrieveDeviceGroupStatus function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDeviceGroupStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveDeviceGroupStatus(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDeviceGroupStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDeviceGroupTriggerInfo - errors', () => {
      it('should have a retrieveDeviceGroupTriggerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDeviceGroupTriggerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveDeviceGroupTriggerInfo(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDeviceGroupTriggerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDeviceGroupsDeviceGroupsById - errors', () => {
      it('should have a deleteIcebergDeviceGroupsDeviceGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDeviceGroupsDeviceGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceGroupsDeviceGroups - errors', () => {
      it('should have a retrieveIcebergDeviceGroupsDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceGroupsDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergDeviceGroupsDeviceGroupsById - errors', () => {
      it('should have a createIcebergDeviceGroupsDeviceGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergDeviceGroupsDeviceGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergDeviceGroupsDeviceGroupsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDeviceGroupsDeviceGroupsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergDeviceGroupsDeviceGroupsById - errors', () => {
      it('should have a updateIcebergDeviceGroupsDeviceGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergDeviceGroupsDeviceGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergDeviceGroupsDeviceGroupsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDeviceGroupsDeviceGroupsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceDevice - errors', () => {
      it('should have a retrieveIcebergDeviceDevice function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDeviceDeviceById - errors', () => {
      it('should have a deleteIcebergDeviceDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDeviceDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.deleteIcebergDeviceDeviceById(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceDeviceById - errors', () => {
      it('should have a retrieveIcebergDeviceDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveIcebergDeviceDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergDeviceDeviceById - errors', () => {
      it('should have a createIcebergDeviceDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergDeviceDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.createIcebergDeviceDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergDeviceDeviceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergDeviceDeviceById - errors', () => {
      it('should have a updateIcebergDeviceDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergDeviceDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.updateIcebergDeviceDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergDeviceDeviceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDeviceDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDevicesDevicesById - errors', () => {
      it('should have a deleteIcebergDevicesDevicesById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDevicesDevicesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDevicesDevices - errors', () => {
      it('should have a retrieveIcebergDevicesDevices function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDevicesDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergDevicesDevicesById - errors', () => {
      it('should have a createIcebergDevicesDevicesById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergDevicesDevicesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergDevicesDevicesById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergDevicesDevicesById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergDevicesDevicesById - errors', () => {
      it('should have a updateIcebergDevicesDevicesById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergDevicesDevicesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergDevicesDevicesById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergDevicesDevicesById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNetworkGroupNetworkGroup - errors', () => {
      it('should have a retrieveIcebergNetworkGroupNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNetworkGroupNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergNetworkGroupNetworkGroupById - errors', () => {
      it('should have a deleteIcebergNetworkGroupNetworkGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergNetworkGroupNetworkGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.deleteIcebergNetworkGroupNetworkGroupById(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNetworkGroupNetworkGroupById - errors', () => {
      it('should have a retrieveIcebergNetworkGroupNetworkGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNetworkGroupNetworkGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveIcebergNetworkGroupNetworkGroupById(null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergNetworkGroupNetworkGroupById - errors', () => {
      it('should have a createIcebergNetworkGroupNetworkGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergNetworkGroupNetworkGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.createIcebergNetworkGroupNetworkGroupById(null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergNetworkGroupNetworkGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergNetworkGroupNetworkGroupById - errors', () => {
      it('should have a updateIcebergNetworkGroupNetworkGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergNetworkGroupNetworkGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.updateIcebergNetworkGroupNetworkGroupById(null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergNetworkGroupNetworkGroupById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveNetworkGroupStatus - errors', () => {
      it('should have a retrieveNetworkGroupStatus function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveNetworkGroupStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveNetworkGroupStatus(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveNetworkGroupStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveNetworkGroupTriggerInfo - errors', () => {
      it('should have a retrieveNetworkGroupTriggerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveNetworkGroupTriggerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveNetworkGroupTriggerInfo(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveNetworkGroupTriggerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergNetworkGroupsNetworkGroupsById - errors', () => {
      it('should have a deleteIcebergNetworkGroupsNetworkGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergNetworkGroupsNetworkGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNetworkGroupsNetworkGroups - errors', () => {
      it('should have a retrieveIcebergNetworkGroupsNetworkGroups function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNetworkGroupsNetworkGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergNetworkGroupsNetworkGroupsById - errors', () => {
      it('should have a createIcebergNetworkGroupsNetworkGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergNetworkGroupsNetworkGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergNetworkGroupsNetworkGroupsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNetworkGroupsNetworkGroupsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergNetworkGroupsNetworkGroupsById - errors', () => {
      it('should have a updateIcebergNetworkGroupsNetworkGroupsById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergNetworkGroupsNetworkGroupsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergNetworkGroupsNetworkGroupsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNetworkGroupsNetworkGroupsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNotificationNotification - errors', () => {
      it('should have a retrieveIcebergNotificationNotification function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNotificationNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergNotificationNotificationById - errors', () => {
      it('should have a deleteIcebergNotificationNotificationById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergNotificationNotificationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationName', (done) => {
        try {
          a.deleteIcebergNotificationNotificationById(null, (data, error) => {
            try {
              const displayE = 'notificationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNotificationNotificationById - errors', () => {
      it('should have a retrieveIcebergNotificationNotificationById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNotificationNotificationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationName', (done) => {
        try {
          a.retrieveIcebergNotificationNotificationById(null, null, (data, error) => {
            try {
              const displayE = 'notificationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergNotificationNotificationById - errors', () => {
      it('should have a createIcebergNotificationNotificationById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergNotificationNotificationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationName', (done) => {
        try {
          a.createIcebergNotificationNotificationById(null, null, (data, error) => {
            try {
              const displayE = 'notificationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergNotificationNotificationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergNotificationNotificationById - errors', () => {
      it('should have a updateIcebergNotificationNotificationById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergNotificationNotificationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationName', (done) => {
        try {
          a.updateIcebergNotificationNotificationById(null, null, (data, error) => {
            try {
              const displayE = 'notificationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergNotificationNotificationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNotificationNotificationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergNotificationsNotificationsById - errors', () => {
      it('should have a deleteIcebergNotificationsNotificationsById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergNotificationsNotificationsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergNotificationsNotificationsById - errors', () => {
      it('should have a retrieveIcebergNotificationsNotificationsById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergNotificationsNotificationsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergNotificationsNotificationsById - errors', () => {
      it('should have a createIcebergNotificationsNotificationsById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergNotificationsNotificationsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergNotificationsNotificationsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergNotificationsNotificationsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergNotificationsNotificationsById - errors', () => {
      it('should have a updateIcebergNotificationsNotificationsById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergNotificationsNotificationsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergNotificationsNotificationsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergNotificationsNotificationsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergPlaybookPlaybook - errors', () => {
      it('should have a retrieveIcebergPlaybookPlaybook function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergPlaybookPlaybook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergPlaybookPlaybookById - errors', () => {
      it('should have a deleteIcebergPlaybookPlaybookById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergPlaybookPlaybookById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing playbookName', (done) => {
        try {
          a.deleteIcebergPlaybookPlaybookById(null, (data, error) => {
            try {
              const displayE = 'playbookName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergPlaybookPlaybookById - errors', () => {
      it('should have a retrieveIcebergPlaybookPlaybookById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergPlaybookPlaybookById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing playbookName', (done) => {
        try {
          a.retrieveIcebergPlaybookPlaybookById(null, null, null, (data, error) => {
            try {
              const displayE = 'playbookName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergPlaybookPlaybookById - errors', () => {
      it('should have a createIcebergPlaybookPlaybookById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergPlaybookPlaybookById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing playbookName', (done) => {
        try {
          a.createIcebergPlaybookPlaybookById(null, null, (data, error) => {
            try {
              const displayE = 'playbookName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergPlaybookPlaybookById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergPlaybookPlaybookById - errors', () => {
      it('should have a updateIcebergPlaybookPlaybookById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergPlaybookPlaybookById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing playbookName', (done) => {
        try {
          a.updateIcebergPlaybookPlaybookById(null, null, (data, error) => {
            try {
              const displayE = 'playbookName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergPlaybookPlaybookById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergPlaybookPlaybookById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergPlaybooksPlaybooksById - errors', () => {
      it('should have a deleteIcebergPlaybooksPlaybooksById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergPlaybooksPlaybooksById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergPlaybooksPlaybooksById - errors', () => {
      it('should have a retrieveIcebergPlaybooksPlaybooksById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergPlaybooksPlaybooksById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergPlaybooksPlaybooksById - errors', () => {
      it('should have a createIcebergPlaybooksPlaybooksById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergPlaybooksPlaybooksById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergPlaybooksPlaybooksById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergPlaybooksPlaybooksById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergPlaybooksPlaybooksById - errors', () => {
      it('should have a updateIcebergPlaybooksPlaybooksById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergPlaybooksPlaybooksById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergPlaybooksPlaybooksById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergPlaybooksPlaybooksById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergRetentionPoliciesRetentionPoliciesById - errors', () => {
      it('should have a deleteIcebergRetentionPoliciesRetentionPoliciesById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergRetentionPoliciesRetentionPoliciesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergRetentionPoliciesRetentionPoliciesById - errors', () => {
      it('should have a retrieveIcebergRetentionPoliciesRetentionPoliciesById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergRetentionPoliciesRetentionPoliciesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergRetentionPoliciesRetentionPoliciesById - errors', () => {
      it('should have a createIcebergRetentionPoliciesRetentionPoliciesById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergRetentionPoliciesRetentionPoliciesById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergRetentionPoliciesRetentionPoliciesById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergRetentionPoliciesRetentionPoliciesById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergRetentionPoliciesRetentionPoliciesId - errors', () => {
      it('should have a updateIcebergRetentionPoliciesRetentionPoliciesId function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergRetentionPoliciesRetentionPoliciesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergRetentionPoliciesRetentionPoliciesId(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergRetentionPoliciesRetentionPoliciesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergRetentionPolicyRetentionPolicy - errors', () => {
      it('should have a retrieveIcebergRetentionPolicyRetentionPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergRetentionPolicyRetentionPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergRetentionPolicyRetentionPolicyById - errors', () => {
      it('should have a deleteIcebergRetentionPolicyRetentionPolicyById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergRetentionPolicyRetentionPolicyById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing retentionPolicyName', (done) => {
        try {
          a.deleteIcebergRetentionPolicyRetentionPolicyById(null, (data, error) => {
            try {
              const displayE = 'retentionPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergRetentionPolicyRetentionPolicyById - errors', () => {
      it('should have a retrieveIcebergRetentionPolicyRetentionPolicyById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergRetentionPolicyRetentionPolicyById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing retentionPolicyName', (done) => {
        try {
          a.retrieveIcebergRetentionPolicyRetentionPolicyById(null, null, (data, error) => {
            try {
              const displayE = 'retentionPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergRetentionPolicyRetentionPolicyById - errors', () => {
      it('should have a createIcebergRetentionPolicyRetentionPolicyById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergRetentionPolicyRetentionPolicyById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing retentionPolicyName', (done) => {
        try {
          a.createIcebergRetentionPolicyRetentionPolicyById(null, null, (data, error) => {
            try {
              const displayE = 'retentionPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergRetentionPolicyRetentionPolicyById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergRetentionPolicyRetentionPolicyById - errors', () => {
      it('should have a updateIcebergRetentionPolicyRetentionPolicyById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergRetentionPolicyRetentionPolicyById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing retentionPolicyName', (done) => {
        try {
          a.updateIcebergRetentionPolicyRetentionPolicyById(null, null, (data, error) => {
            try {
              const displayE = 'retentionPolicyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergRetentionPolicyRetentionPolicyById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergRetentionPolicyRetentionPolicyById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSystemById - errors', () => {
      it('should have a deleteIcebergSystemSystemById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSystemById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSystem - errors', () => {
      it('should have a retrieveIcebergSystemSystem function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSystemById - errors', () => {
      it('should have a createIcebergSystemSystemById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSystemById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSystemById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSystemById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSystemById - errors', () => {
      it('should have a updateIcebergSystemSystemById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSystemById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSystemById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSystemById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSchedulers - errors', () => {
      it('should have a deleteIcebergSystemSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSchedulers - errors', () => {
      it('should have a retrieveIcebergSystemSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSchedulers - errors', () => {
      it('should have a createIcebergSystemSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSchedulers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSchedulers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSchedulers - errors', () => {
      it('should have a updateIcebergSystemSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSchedulers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSchedulers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSchedulerById - errors', () => {
      it('should have a deleteIcebergSystemSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemSchedulerById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSchedulerById - errors', () => {
      it('should have a retrieveIcebergSystemSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSchedulerById - errors', () => {
      it('should have a createIcebergSystemSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSchedulerById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSchedulerById - errors', () => {
      it('should have a updateIcebergSystemSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSchedulerById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemDestinations - errors', () => {
      it('should have a deleteIcebergSystemDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemDestinations - errors', () => {
      it('should have a retrieveIcebergSystemDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemDestinations - errors', () => {
      it('should have a createIcebergSystemDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemDestinations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemDestinations - errors', () => {
      it('should have a updateIcebergSystemDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemDestinations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemDestinationById - errors', () => {
      it('should have a deleteIcebergSystemDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemDestinationById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemDestinationById - errors', () => {
      it('should have a retrieveIcebergSystemDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemDestinationById - errors', () => {
      it('should have a createIcebergSystemDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemDestinationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemDestinationById - errors', () => {
      it('should have a updateIcebergSystemDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemDestinationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemReports - errors', () => {
      it('should have a deleteIcebergSystemReports function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemReports - errors', () => {
      it('should have a retrieveIcebergSystemReports function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemReports - errors', () => {
      it('should have a createIcebergSystemReports function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemReports(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemReports - errors', () => {
      it('should have a updateIcebergSystemReports function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemReports(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemReportById - errors', () => {
      it('should have a deleteIcebergSystemReportById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemReportById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemReportById - errors', () => {
      it('should have a retrieveIcebergSystemReportById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemReportById - errors', () => {
      it('should have a createIcebergSystemReportById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemReportById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemReportById - errors', () => {
      it('should have a updateIcebergSystemReportById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemReportById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsSystemSettingsById - errors', () => {
      it('should have a deleteIcebergSystemSettingsSystemSettingsById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsSystemSettingsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsSystemSettings - errors', () => {
      it('should have a retrieveIcebergSystemSettingsSystemSettings function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsSystemSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsSystemSettingsById - errors', () => {
      it('should have a createIcebergSystemSettingsSystemSettingsById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsSystemSettingsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsSystemSettingsById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsSystemSettingsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsSystemSettingsById - errors', () => {
      it('should have a updateIcebergSystemSettingsSystemSettingsById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsSystemSettingsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsSystemSettingsById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsSystemSettingsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsSchedulers - errors', () => {
      it('should have a deleteIcebergSystemSettingsSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsSchedulers - errors', () => {
      it('should have a retrieveIcebergSystemSettingsSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsSchedulers - errors', () => {
      it('should have a createIcebergSystemSettingsSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsSchedulers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsSchedulers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsSchedulers - errors', () => {
      it('should have a updateIcebergSystemSettingsSchedulers function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsSchedulers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsSchedulers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsSchedulers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsSchedulerById - errors', () => {
      it('should have a deleteIcebergSystemSettingsSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemSettingsSchedulerById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsSchedulerById - errors', () => {
      it('should have a retrieveIcebergSystemSettingsSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemSettingsSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsSchedulerById - errors', () => {
      it('should have a createIcebergSystemSettingsSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemSettingsSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsSchedulerById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsSchedulerById - errors', () => {
      it('should have a updateIcebergSystemSettingsSchedulerById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsSchedulerById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemSettingsSchedulerById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsSchedulerById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsSchedulerById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsDestinations - errors', () => {
      it('should have a deleteIcebergSystemSettingsDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsDestinations - errors', () => {
      it('should have a retrieveIcebergSystemSettingsDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsDestinations - errors', () => {
      it('should have a createIcebergSystemSettingsDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsDestinations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsDestinations - errors', () => {
      it('should have a updateIcebergSystemSettingsDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsDestinations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsDestinationById - errors', () => {
      it('should have a deleteIcebergSystemSettingsDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemSettingsDestinationById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsDestinationById - errors', () => {
      it('should have a retrieveIcebergSystemSettingsDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemSettingsDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsDestinationById - errors', () => {
      it('should have a createIcebergSystemSettingsDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemSettingsDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsDestinationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsDestinationById - errors', () => {
      it('should have a updateIcebergSystemSettingsDestinationById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsDestinationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemSettingsDestinationById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsDestinationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsDestinationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsReports - errors', () => {
      it('should have a deleteIcebergSystemSettingsReports function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsReports - errors', () => {
      it('should have a retrieveIcebergSystemSettingsReports function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsReports - errors', () => {
      it('should have a createIcebergSystemSettingsReports function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsReports(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsReports - errors', () => {
      it('should have a updateIcebergSystemSettingsReports function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsReports === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsReports(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsReports', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergSystemSettingsReportById - errors', () => {
      it('should have a deleteIcebergSystemSettingsReportById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergSystemSettingsReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergSystemSettingsReportById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergSystemSettingsReportById - errors', () => {
      it('should have a retrieveIcebergSystemSettingsReportById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergSystemSettingsReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergSystemSettingsReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergSystemSettingsReportById - errors', () => {
      it('should have a createIcebergSystemSettingsReportById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergSystemSettingsReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergSystemSettingsReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergSystemSettingsReportById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergSystemSettingsReportById - errors', () => {
      it('should have a updateIcebergSystemSettingsReportById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergSystemSettingsReportById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergSystemSettingsReportById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergSystemSettingsReportById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergSystemSettingsReportById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergTopicTopic - errors', () => {
      it('should have a retrieveIcebergTopicTopic function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergTopicTopic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergTopicTopicById - errors', () => {
      it('should have a deleteIcebergTopicTopicById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergTopicTopicById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.deleteIcebergTopicTopicById(null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergTopicTopicById - errors', () => {
      it('should have a retrieveIcebergTopicTopicById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergTopicTopicById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.retrieveIcebergTopicTopicById(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergTopicTopicById - errors', () => {
      it('should have a createIcebergTopicTopicById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergTopicTopicById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.createIcebergTopicTopicById(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergTopicTopicById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergTopicTopicById - errors', () => {
      it('should have a updateIcebergTopicTopicById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergTopicTopicById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.updateIcebergTopicTopicById(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergTopicTopicById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicTopicById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergTopicRuleRule - errors', () => {
      it('should have a retrieveIcebergTopicRuleRule function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergTopicRuleRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.retrieveIcebergTopicRuleRule(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergTopicRuleRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergTopicRuleRuleById - errors', () => {
      it('should have a deleteIcebergTopicRuleRuleById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergTopicRuleRuleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.deleteIcebergTopicRuleRuleById(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.deleteIcebergTopicRuleRuleById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergTopicRuleRuleById - errors', () => {
      it('should have a retrieveIcebergTopicRuleRuleById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergTopicRuleRuleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.retrieveIcebergTopicRuleRuleById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.retrieveIcebergTopicRuleRuleById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergTopicRuleRuleById - errors', () => {
      it('should have a createIcebergTopicRuleRuleById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergTopicRuleRuleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.createIcebergTopicRuleRuleById(null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.createIcebergTopicRuleRuleById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergTopicRuleRuleById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergTopicRuleRuleById - errors', () => {
      it('should have a updateIcebergTopicRuleRuleById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergTopicRuleRuleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.updateIcebergTopicRuleRuleById(null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleName', (done) => {
        try {
          a.updateIcebergTopicRuleRuleById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergTopicRuleRuleById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicRuleRuleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotTopicResourceResourceById - errors', () => {
      it('should have a deleteHealthbotTopicResourceResourceById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotTopicResourceResourceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.deleteHealthbotTopicResourceResourceById(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceName', (done) => {
        try {
          a.deleteHealthbotTopicResourceResourceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'resourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotTopicResourceResourceById - errors', () => {
      it('should have a createHealthbotTopicResourceResourceById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotTopicResourceResourceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.createHealthbotTopicResourceResourceById(null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceName', (done) => {
        try {
          a.createHealthbotTopicResourceResourceById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotTopicResourceResourceById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotTopicResourceResourceById - errors', () => {
      it('should have a updateHealthbotTopicResourceResourceById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotTopicResourceResourceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.updateHealthbotTopicResourceResourceById(null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceName', (done) => {
        try {
          a.updateHealthbotTopicResourceResourceById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotTopicResourceResourceById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergTopicsTopicsById - errors', () => {
      it('should have a deleteIcebergTopicsTopicsById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergTopicsTopicsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergTopicsTopics - errors', () => {
      it('should have a retrieveIcebergTopicsTopics function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergTopicsTopics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergTopicsTopicsById - errors', () => {
      it('should have a createIcebergTopicsTopicsById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergTopicsTopicsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergTopicsTopicsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergTopicsTopicsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergTopicsTopicsById - errors', () => {
      it('should have a updateIcebergTopicsTopicsById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergTopicsTopicsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergTopicsTopicsById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergTopicsTopicsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirstLogin - errors', () => {
      it('should have a postFirstLogin function', (done) => {
        try {
          assert.equal(true, typeof a.postFirstLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postFirstLogin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-postFirstLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotOrganizationsOrganizations - errors', () => {
      it('should have a deleteHealthbotOrganizationsOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotOrganizationsOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotOrganizationsOrganizations - errors', () => {
      it('should have a retrieveHealthbotOrganizationsOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotOrganizationsOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotOrganizationsOrganizations - errors', () => {
      it('should have a createHealthbotOrganizationsOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotOrganizationsOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotOrganizationsOrganizations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationsOrganizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotOrganizationsOrganizations - errors', () => {
      it('should have a updateHealthbotOrganizationsOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotOrganizationsOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotOrganizationsOrganizations(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationsOrganizations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiIngestMappings - errors', () => {
      it('should have a retrieveHealthbotIngestByoiIngestMappings function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiIngestMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiIngestMappings - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiIngestMappings function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiIngestMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotTopicResourceResourceById - errors', () => {
      it('should have a retrieveHealthbotTopicResourceResourceById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotTopicResourceResourceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.retrieveHealthbotTopicResourceResourceById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceName', (done) => {
        try {
          a.retrieveHealthbotTopicResourceResourceById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'resourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotTopicResourceResourceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngest - errors', () => {
      it('should have a deleteIcebergIngest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngest - errors', () => {
      it('should have a retrieveIcebergIngest function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngest - errors', () => {
      it('should have a createIcebergIngest function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngest - errors', () => {
      it('should have a updateIcebergIngest function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngest(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestTaggingProfiles - errors', () => {
      it('should have a retrieveHealthbotIngestTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestTaggingProfiles - errors', () => {
      it('should have a deleteHealthbotIngestTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestTaggingProfiles - errors', () => {
      it('should have a createHealthbotIngestTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestTaggingProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestTaggingProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestTaggingProfiles - errors', () => {
      it('should have a updateHealthbotIngestTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestTaggingProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestTaggingProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestTaggingProfileById - errors', () => {
      it('should have a deleteHealthbotIngestTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestTaggingProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestTaggingProfileById - errors', () => {
      it('should have a retrieveHealthbotIngestTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestTaggingProfileById - errors', () => {
      it('should have a createHealthbotIngestTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestTaggingProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestTaggingProfileById - errors', () => {
      it('should have a updateHealthbotIngestTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestTaggingProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestFlow - errors', () => {
      it('should have a deleteIcebergIngestFlow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestFlow - errors', () => {
      it('should have a retrieveIcebergIngestFlow function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestFlow - errors', () => {
      it('should have a createIcebergIngestFlow function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestFlow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestFlow - errors', () => {
      it('should have a updateIcebergIngestFlow function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestFlow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestFlowTemplateIds - errors', () => {
      it('should have a retrieveIcebergIngestFlowTemplateIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestFlowTemplateIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestFlowTemplateById - errors', () => {
      it('should have a deleteIcebergIngestFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestFlowTemplateById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestFlowTemplateById - errors', () => {
      it('should have a retrieveIcebergIngestFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestFlowTemplateById - errors', () => {
      it('should have a createIcebergIngestFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestFlowTemplateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestFlowTemplateById - errors', () => {
      it('should have a updateIcebergIngestFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestFlowTemplateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSflow - errors', () => {
      it('should have a deleteHealthbotIngestSflow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSflow - errors', () => {
      it('should have a retrieveHealthbotIngestSflow function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSflow - errors', () => {
      it('should have a createHealthbotIngestSflow function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSflow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSflow - errors', () => {
      it('should have a updateHealthbotIngestSflow function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSflow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSflowCounterRecordById - errors', () => {
      it('should have a deleteHealthbotIngestSflowCounterRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSflowCounterRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.deleteHealthbotIngestSflowCounterRecordById(null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSflowCounterRecordById - errors', () => {
      it('should have a retrieveHealthbotIngestSflowCounterRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSflowCounterRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.retrieveHealthbotIngestSflowCounterRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSflowCounterRecordById - errors', () => {
      it('should have a createHealthbotIngestSflowCounterRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSflowCounterRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.createHealthbotIngestSflowCounterRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSflowCounterRecordById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSflowCounterRecordById - errors', () => {
      it('should have a updateHealthbotIngestSflowCounterRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSflowCounterRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.updateHealthbotIngestSflowCounterRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSflowCounterRecordById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowCounterRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSflowFlowRecordById - errors', () => {
      it('should have a deleteHealthbotIngestSflowFlowRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSflowFlowRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.deleteHealthbotIngestSflowFlowRecordById(null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSflowFlowRecordById - errors', () => {
      it('should have a retrieveHealthbotIngestSflowFlowRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSflowFlowRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.retrieveHealthbotIngestSflowFlowRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSflowFlowRecordById - errors', () => {
      it('should have a createHealthbotIngestSflowFlowRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSflowFlowRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.createHealthbotIngestSflowFlowRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSflowFlowRecordById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSflowFlowRecordById - errors', () => {
      it('should have a updateHealthbotIngestSflowFlowRecordById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSflowFlowRecordById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordName', (done) => {
        try {
          a.updateHealthbotIngestSflowFlowRecordById(null, null, (data, error) => {
            try {
              const displayE = 'recordName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSflowFlowRecordById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowFlowRecordById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSflowSampleById - errors', () => {
      it('should have a deleteHealthbotIngestSflowSampleById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSflowSampleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sampleName', (done) => {
        try {
          a.deleteHealthbotIngestSflowSampleById(null, (data, error) => {
            try {
              const displayE = 'sampleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSflowSampleById - errors', () => {
      it('should have a retrieveHealthbotIngestSflowSampleById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSflowSampleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sampleName', (done) => {
        try {
          a.retrieveHealthbotIngestSflowSampleById(null, null, (data, error) => {
            try {
              const displayE = 'sampleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSflowSampleById - errors', () => {
      it('should have a createHealthbotIngestSflowSampleById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSflowSampleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sampleName', (done) => {
        try {
          a.createHealthbotIngestSflowSampleById(null, null, (data, error) => {
            try {
              const displayE = 'sampleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSflowSampleById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSflowSampleById - errors', () => {
      it('should have a updateHealthbotIngestSflowSampleById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSflowSampleById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sampleName', (done) => {
        try {
          a.updateHealthbotIngestSflowSampleById(null, null, (data, error) => {
            try {
              const displayE = 'sampleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSflowSampleById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowSampleById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSflowProtocolById - errors', () => {
      it('should have a deleteHealthbotIngestSflowProtocolById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSflowProtocolById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocolName', (done) => {
        try {
          a.deleteHealthbotIngestSflowProtocolById(null, (data, error) => {
            try {
              const displayE = 'protocolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSflowProtocolById - errors', () => {
      it('should have a retrieveHealthbotIngestSflowProtocolById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSflowProtocolById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocolName', (done) => {
        try {
          a.retrieveHealthbotIngestSflowProtocolById(null, null, (data, error) => {
            try {
              const displayE = 'protocolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSflowProtocolById - errors', () => {
      it('should have a createHealthbotIngestSflowProtocolById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSflowProtocolById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocolName', (done) => {
        try {
          a.createHealthbotIngestSflowProtocolById(null, null, (data, error) => {
            try {
              const displayE = 'protocolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSflowProtocolById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSflowProtocolById - errors', () => {
      it('should have a updateHealthbotIngestSflowProtocolById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSflowProtocolById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocolName', (done) => {
        try {
          a.updateHealthbotIngestSflowProtocolById(null, null, (data, error) => {
            try {
              const displayE = 'protocolName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSflowProtocolById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSflowProtocolById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestFrequencyProfile - errors', () => {
      it('should have a retrieveHealthbotIngestFrequencyProfile function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestFrequencyProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestFrequencyProfileById - errors', () => {
      it('should have a deleteHealthbotIngestFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestFrequencyProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestFrequencyProfileById - errors', () => {
      it('should have a retrieveHealthbotIngestFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestFrequencyProfileById - errors', () => {
      it('should have a createHealthbotIngestFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestFrequencyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestFrequencyProfileById - errors', () => {
      it('should have a updateHealthbotIngestFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestFrequencyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestNativeGpb - errors', () => {
      it('should have a deleteIcebergIngestNativeGpb function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestNativeGpb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestNativeGpb - errors', () => {
      it('should have a retrieveIcebergIngestNativeGpb function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestNativeGpb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestNativeGpb - errors', () => {
      it('should have a createIcebergIngestNativeGpb function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestNativeGpb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestNativeGpb(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestNativeGpb', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestNativeGpb - errors', () => {
      it('should have a updateIcebergIngestNativeGpb function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestNativeGpb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestNativeGpb(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestNativeGpb', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestOutboundSsh - errors', () => {
      it('should have a deleteHealthbotIngestOutboundSsh function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestOutboundSsh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestOutboundSsh - errors', () => {
      it('should have a retrieveHealthbotIngestOutboundSsh function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestOutboundSsh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestOutboundSsh - errors', () => {
      it('should have a createHealthbotIngestOutboundSsh function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestOutboundSsh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestOutboundSsh(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestOutboundSsh', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestOutboundSsh - errors', () => {
      it('should have a updateHealthbotIngestOutboundSsh function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestOutboundSsh === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestOutboundSsh(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestOutboundSsh', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSnmpNotification - errors', () => {
      it('should have a deleteHealthbotIngestSnmpNotification function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSnmpNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSnmpNotification - errors', () => {
      it('should have a retrieveHealthbotIngestSnmpNotification function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSnmpNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSnmpNotification - errors', () => {
      it('should have a createHealthbotIngestSnmpNotification function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSnmpNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSnmpNotification(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSnmpNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSnmpNotification - errors', () => {
      it('should have a updateHealthbotIngestSnmpNotification function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSnmpNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSnmpNotification(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSnmpNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSnmpNotificationV3UsmUsernames - errors', () => {
      it('should have a retrieveHealthbotIngestSnmpNotificationV3UsmUsernames function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSnmpNotificationV3UsmUsernames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSnmpNotificationV3UsmUsers - errors', () => {
      it('should have a retrieveHealthbotIngestSnmpNotificationV3UsmUsers function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSnmpNotificationV3UsmUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSnmpNotificationV3UsmUserById - errors', () => {
      it('should have a deleteHealthbotIngestSnmpNotificationV3UsmUserById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSnmpNotificationV3UsmUserById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSnmpNotificationV3UsmUserById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSnmpNotificationV3UsmUserById - errors', () => {
      it('should have a retrieveHealthbotIngestSnmpNotificationV3UsmUserById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSnmpNotificationV3UsmUserById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSnmpNotificationV3UsmUserById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSnmpNotificationV3UsmUserById - errors', () => {
      it('should have a createHealthbotIngestSnmpNotificationV3UsmUserById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSnmpNotificationV3UsmUserById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSnmpNotificationV3UsmUserById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSnmpNotificationV3UsmUserById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSnmpNotificationV3UsmUserById - errors', () => {
      it('should have a updateHealthbotIngestSnmpNotificationV3UsmUserById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSnmpNotificationV3UsmUserById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSnmpNotificationV3UsmUserById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSnmpNotificationV3UsmUserById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSnmpNotificationV3UsmUserById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSyslog - errors', () => {
      it('should have a deleteIcebergIngestSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslog - errors', () => {
      it('should have a retrieveIcebergIngestSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSyslog - errors', () => {
      it('should have a createIcebergIngestSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSyslog(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSyslog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSyslog - errors', () => {
      it('should have a updateIcebergIngestSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSyslog(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSyslog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatternSetIds - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatternSetIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatternSetIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatternSets - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatternSets function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatternSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSyslogPatternSetById - errors', () => {
      it('should have a deleteIcebergIngestSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestSyslogPatternSetById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatternSetById - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSyslogPatternSetById - errors', () => {
      it('should have a createIcebergIngestSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSyslogPatternSetById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSyslogPatternSetById - errors', () => {
      it('should have a updateIcebergIngestSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSyslogPatternSetById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatternIds - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatternIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatternIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatterns - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatterns function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatterns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSyslogPatternById - errors', () => {
      it('should have a deleteIcebergIngestSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestSyslogPatternById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSyslogPatternById - errors', () => {
      it('should have a retrieveIcebergIngestSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSyslogPatternById - errors', () => {
      it('should have a createIcebergIngestSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSyslogPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSyslogPatternById - errors', () => {
      it('should have a updateIcebergIngestSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSyslogPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSyslogHeaderPatternIds - errors', () => {
      it('should have a retrieveHealthbotIngestSyslogHeaderPatternIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSyslogHeaderPatternIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSyslogHeaderPatterns - errors', () => {
      it('should have a retrieveHealthbotIngestSyslogHeaderPatterns function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSyslogHeaderPatterns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSyslogHeaderPatternById - errors', () => {
      it('should have a deleteHealthbotIngestSyslogHeaderPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSyslogHeaderPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSyslogHeaderPatternById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSyslogHeaderPatternById - errors', () => {
      it('should have a retrieveHealthbotIngestSyslogHeaderPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSyslogHeaderPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSyslogHeaderPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSyslogHeaderPatternById - errors', () => {
      it('should have a createHealthbotIngestSyslogHeaderPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSyslogHeaderPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSyslogHeaderPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSyslogHeaderPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSyslogHeaderPatternById - errors', () => {
      it('should have a updateHealthbotIngestSyslogHeaderPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSyslogHeaderPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSyslogHeaderPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSyslogHeaderPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSyslogHeaderPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiCustomPlugins - errors', () => {
      it('should have a retrieveHealthbotIngestByoiCustomPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiCustomPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestByoiCustomPluginById - errors', () => {
      it('should have a deleteHealthbotIngestByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestByoiCustomPluginById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiCustomPluginById - errors', () => {
      it('should have a retrieveHealthbotIngestByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestByoiCustomPluginById - errors', () => {
      it('should have a createHealthbotIngestByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestByoiCustomPluginById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestByoiCustomPluginById - errors', () => {
      it('should have a updateHealthbotIngestByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestByoiCustomPluginById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiDefaultPluginTliveKafkas - errors', () => {
      it('should have a retrieveHealthbotIngestByoiDefaultPluginTliveKafkas function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiDefaultPluginTliveKafkas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a deleteHealthbotIngestByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestByoiDefaultPluginTliveKafkaById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a createHealthbotIngestByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestByoiDefaultPluginTliveKafkaById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a updateHealthbotIngestByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestByoiDefaultPluginTliveKafkaById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestByoiIngestMappingById - errors', () => {
      it('should have a deleteHealthbotIngestByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestByoiIngestMappingById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestByoiIngestMappingById - errors', () => {
      it('should have a retrieveHealthbotIngestByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestByoiIngestMappingById - errors', () => {
      it('should have a createHealthbotIngestByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestByoiIngestMappingById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestByoiIngestMappingById - errors', () => {
      it('should have a updateHealthbotIngestByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestByoiIngestMappingById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettings - errors', () => {
      it('should have a deleteIcebergIngestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettings - errors', () => {
      it('should have a retrieveIcebergIngestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettings - errors', () => {
      it('should have a createIcebergIngestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettings - errors', () => {
      it('should have a updateIcebergIngestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsTaggingProfiles - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsTaggingProfiles - errors', () => {
      it('should have a deleteHealthbotIngestSettingsTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsTaggingProfiles - errors', () => {
      it('should have a createHealthbotIngestSettingsTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsTaggingProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsTaggingProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsTaggingProfiles - errors', () => {
      it('should have a updateHealthbotIngestSettingsTaggingProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsTaggingProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsTaggingProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsTaggingProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsTaggingProfileById - errors', () => {
      it('should have a deleteHealthbotIngestSettingsTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSettingsTaggingProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsTaggingProfileById - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSettingsTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsTaggingProfileById - errors', () => {
      it('should have a createHealthbotIngestSettingsTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSettingsTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsTaggingProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsTaggingProfileById - errors', () => {
      it('should have a updateHealthbotIngestSettingsTaggingProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsTaggingProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSettingsTaggingProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsTaggingProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsTaggingProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettingsFlow - errors', () => {
      it('should have a deleteIcebergIngestSettingsFlow function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettingsFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsFlow - errors', () => {
      it('should have a retrieveIcebergIngestSettingsFlow function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettingsFlow - errors', () => {
      it('should have a createIcebergIngestSettingsFlow function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettingsFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettingsFlow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettingsFlow - errors', () => {
      it('should have a updateIcebergIngestSettingsFlow function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettingsFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettingsFlow(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsFlowTemplateIds - errors', () => {
      it('should have a retrieveIcebergIngestSettingsFlowTemplateIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsFlowTemplateIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettingsFlowTemplateById - errors', () => {
      it('should have a deleteIcebergIngestSettingsFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettingsFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestSettingsFlowTemplateById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsFlowTemplateById - errors', () => {
      it('should have a retrieveIcebergIngestSettingsFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestSettingsFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettingsFlowTemplateById - errors', () => {
      it('should have a createIcebergIngestSettingsFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettingsFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestSettingsFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettingsFlowTemplateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettingsFlowTemplateById - errors', () => {
      it('should have a updateIcebergIngestSettingsFlowTemplateById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettingsFlowTemplateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestSettingsFlowTemplateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettingsFlowTemplateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsFlowTemplateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsFrequencyProfile - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsFrequencyProfile function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsFrequencyProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsFrequencyProfileById - errors', () => {
      it('should have a deleteHealthbotIngestSettingsFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSettingsFrequencyProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsFrequencyProfileById - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSettingsFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsFrequencyProfileById - errors', () => {
      it('should have a createHealthbotIngestSettingsFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSettingsFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsFrequencyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsFrequencyProfileById - errors', () => {
      it('should have a updateHealthbotIngestSettingsFrequencyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsFrequencyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSettingsFrequencyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsFrequencyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsFrequencyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettingsSyslog - errors', () => {
      it('should have a deleteIcebergIngestSettingsSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettingsSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslog - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettingsSyslog - errors', () => {
      it('should have a createIcebergIngestSettingsSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettingsSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettingsSyslog(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsSyslog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettingsSyslog - errors', () => {
      it('should have a updateIcebergIngestSettingsSyslog function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettingsSyslog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettingsSyslog(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsSyslog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatternSetIds - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatternSetIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatternSetIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatternSets - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatternSets function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatternSets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettingsSyslogPatternSetById - errors', () => {
      it('should have a deleteIcebergIngestSettingsSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettingsSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestSettingsSyslogPatternSetById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatternSetById - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestSettingsSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettingsSyslogPatternSetById - errors', () => {
      it('should have a createIcebergIngestSettingsSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettingsSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestSettingsSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettingsSyslogPatternSetById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettingsSyslogPatternSetById - errors', () => {
      it('should have a updateIcebergIngestSettingsSyslogPatternSetById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettingsSyslogPatternSetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestSettingsSyslogPatternSetById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettingsSyslogPatternSetById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsSyslogPatternSetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatternIds - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatternIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatternIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatterns - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatterns function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatterns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergIngestSettingsSyslogPatternById - errors', () => {
      it('should have a deleteIcebergIngestSettingsSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergIngestSettingsSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergIngestSettingsSyslogPatternById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergIngestSettingsSyslogPatternById - errors', () => {
      it('should have a retrieveIcebergIngestSettingsSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergIngestSettingsSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergIngestSettingsSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergIngestSettingsSyslogPatternById - errors', () => {
      it('should have a createIcebergIngestSettingsSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergIngestSettingsSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergIngestSettingsSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergIngestSettingsSyslogPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergIngestSettingsSyslogPatternById - errors', () => {
      it('should have a updateIcebergIngestSettingsSyslogPatternById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergIngestSettingsSyslogPatternById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergIngestSettingsSyslogPatternById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergIngestSettingsSyslogPatternById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergIngestSettingsSyslogPatternById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiCustomPlugins - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiCustomPlugins function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiCustomPlugins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsByoiCustomPluginById - errors', () => {
      it('should have a deleteHealthbotIngestSettingsByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSettingsByoiCustomPluginById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiCustomPluginById - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSettingsByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsByoiCustomPluginById - errors', () => {
      it('should have a createHealthbotIngestSettingsByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiCustomPluginById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsByoiCustomPluginById - errors', () => {
      it('should have a updateHealthbotIngestSettingsByoiCustomPluginById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsByoiCustomPluginById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiCustomPluginById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiCustomPluginById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiCustomPluginById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkas - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkas function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById - errors', () => {
      it('should have a updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestSettingsByoiIngestMappingById - errors', () => {
      it('should have a deleteHealthbotIngestSettingsByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestSettingsByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteHealthbotIngestSettingsByoiIngestMappingById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsByoiIngestMappingById - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveHealthbotIngestSettingsByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestSettingsByoiIngestMappingById - errors', () => {
      it('should have a createHealthbotIngestSettingsByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestSettingsByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestSettingsByoiIngestMappingById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestSettingsByoiIngestMappingById - errors', () => {
      it('should have a updateHealthbotIngestSettingsByoiIngestMappingById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestSettingsByoiIngestMappingById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiIngestMappingById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestSettingsByoiIngestMappingById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestSettingsByoiIngestMappingById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotDeploymentDeploymentById - errors', () => {
      it('should have a deleteHealthbotDeploymentDeploymentById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotDeploymentDeploymentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotDeploymentDeployment - errors', () => {
      it('should have a retrieveHealthbotDeploymentDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotDeploymentDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotDeploymentDeploymentById - errors', () => {
      it('should have a createHealthbotDeploymentDeploymentById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotDeploymentDeploymentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotDeploymentDeploymentById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotDeploymentDeploymentById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotDeploymentDeploymentById - errors', () => {
      it('should have a updateHealthbotDeploymentDeploymentById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotDeploymentDeploymentById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotDeploymentDeploymentById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotDeploymentDeploymentById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveConfigurationJobs - errors', () => {
      it('should have a retrieveConfigurationJobs function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveConfigurationJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveFilesHelperFiles - errors', () => {
      it('should have a retrieveFilesHelperFiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveFilesHelperFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilesHelperFilesByFileName - errors', () => {
      it('should have a deleteFilesHelperFilesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFilesHelperFilesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.deleteFilesHelperFilesByFileName(null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteFilesHelperFilesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveFilesHelperFilesByFileName - errors', () => {
      it('should have a retrieveFilesHelperFilesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveFilesHelperFilesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.retrieveFilesHelperFilesByFileName(null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveFilesHelperFilesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFilesHelperFilesByFileName - errors', () => {
      it('should have a createFilesHelperFilesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.createFilesHelperFilesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.createFilesHelperFilesByFileName(null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createFilesHelperFilesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing upFile', (done) => {
        try {
          a.createFilesHelperFilesByFileName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'upFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createFilesHelperFilesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupHelperFiles - errors', () => {
      it('should have a backupHelperFiles function', (done) => {
        try {
          assert.equal(true, typeof a.backupHelperFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreHelperFiles - errors', () => {
      it('should have a restoreHelperFiles function', (done) => {
        try {
          assert.equal(true, typeof a.restoreHelperFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing restoreFile', (done) => {
        try {
          a.restoreHelperFiles(null, (data, error) => {
            try {
              const displayE = 'restoreFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-restoreHelperFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFilesCertificatesByFileName - errors', () => {
      it('should have a deleteFilesCertificatesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFilesCertificatesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.deleteFilesCertificatesByFileName(null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteFilesCertificatesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveFilesCertificatesByFileName - errors', () => {
      it('should have a retrieveFilesCertificatesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveFilesCertificatesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.retrieveFilesCertificatesByFileName(null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveFilesCertificatesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFilesCertificatesByFileName - errors', () => {
      it('should have a createFilesCertificatesByFileName function', (done) => {
        try {
          assert.equal(true, typeof a.createFilesCertificatesByFileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.createFilesCertificatesByFileName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createFilesCertificatesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing upFile', (done) => {
        try {
          a.createFilesCertificatesByFileName('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'upFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createFilesCertificatesByFileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotDynamicTagging - errors', () => {
      it('should have a retrieveHealthbotDynamicTagging function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotDynamicTagging === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotDynamicTagging - errors', () => {
      it('should have a deleteHealthbotDynamicTagging function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotDynamicTagging === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotDynamicTagging - errors', () => {
      it('should have a createHealthbotDynamicTagging function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotDynamicTagging === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotDynamicTagging(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotDynamicTagging', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotDynamicTagging - errors', () => {
      it('should have a updateHealthbotDynamicTagging function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotDynamicTagging === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotDynamicTagging(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotDynamicTagging', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicTaggingByKey - errors', () => {
      it('should have a getDynamicTaggingByKey function', (done) => {
        try {
          assert.equal(true, typeof a.getDynamicTaggingByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.getDynamicTaggingByKey(null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-getDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDynamicTaggingByKey - errors', () => {
      it('should have a createDynamicTaggingByKey function', (done) => {
        try {
          assert.equal(true, typeof a.createDynamicTaggingByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.createDynamicTaggingByKey(null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDynamicTaggingByKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDynamicTaggingByKey - errors', () => {
      it('should have a deleteDynamicTaggingByKey function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDynamicTaggingByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.deleteDynamicTaggingByKey(null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDynamicTaggingByKey - errors', () => {
      it('should have a updateDynamicTaggingByKey function', (done) => {
        try {
          assert.equal(true, typeof a.updateDynamicTaggingByKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.updateDynamicTaggingByKey(null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDynamicTaggingByKey('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateDynamicTaggingByKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergProfiles - errors', () => {
      it('should have a deleteIcebergProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfiles - errors', () => {
      it('should have a retrieveIcebergProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergProfiles - errors', () => {
      it('should have a createIcebergProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergProfiles - errors', () => {
      it('should have a updateIcebergProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergProfiles(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecurityCaProfiles - errors', () => {
      it('should have a retrieveIcebergProfileSecurityCaProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecurityCaProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergProfileSecurityCaProfileById - errors', () => {
      it('should have a deleteIcebergProfileSecurityCaProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergProfileSecurityCaProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergProfileSecurityCaProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecurityCaProfileById - errors', () => {
      it('should have a retrieveIcebergProfileSecurityCaProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecurityCaProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergProfileSecurityCaProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergProfileSecurityCaProfileById - errors', () => {
      it('should have a createIcebergProfileSecurityCaProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergProfileSecurityCaProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergProfileSecurityCaProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergProfileSecurityCaProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergProfileSecurityCaProfileById - errors', () => {
      it('should have a updateIcebergProfileSecurityCaProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergProfileSecurityCaProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergProfileSecurityCaProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergProfileSecurityCaProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecurityCaProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecurityLocalCertificates - errors', () => {
      it('should have a retrieveIcebergProfileSecurityLocalCertificates function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecurityLocalCertificates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergProfileSecurityLocalCertificateById - errors', () => {
      it('should have a deleteIcebergProfileSecurityLocalCertificateById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergProfileSecurityLocalCertificateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergProfileSecurityLocalCertificateById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecurityLocalCertificateById - errors', () => {
      it('should have a retrieveIcebergProfileSecurityLocalCertificateById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecurityLocalCertificateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergProfileSecurityLocalCertificateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergProfileSecurityLocalCertificateById - errors', () => {
      it('should have a createIcebergProfileSecurityLocalCertificateById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergProfileSecurityLocalCertificateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergProfileSecurityLocalCertificateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergProfileSecurityLocalCertificateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergProfileSecurityLocalCertificateById - errors', () => {
      it('should have a updateIcebergProfileSecurityLocalCertificateById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergProfileSecurityLocalCertificateById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergProfileSecurityLocalCertificateById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergProfileSecurityLocalCertificateById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecurityLocalCertificateById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecuritySshKeyProfiles - errors', () => {
      it('should have a retrieveIcebergProfileSecuritySshKeyProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecuritySshKeyProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergProfileSecuritySshKeyProfileById - errors', () => {
      it('should have a deleteIcebergProfileSecuritySshKeyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergProfileSecuritySshKeyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergProfileSecuritySshKeyProfileById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileSecuritySshKeyProfileById - errors', () => {
      it('should have a retrieveIcebergProfileSecuritySshKeyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileSecuritySshKeyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergProfileSecuritySshKeyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergProfileSecuritySshKeyProfileById - errors', () => {
      it('should have a createIcebergProfileSecuritySshKeyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergProfileSecuritySshKeyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergProfileSecuritySshKeyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergProfileSecuritySshKeyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergProfileSecuritySshKeyProfileById - errors', () => {
      it('should have a updateIcebergProfileSecuritySshKeyProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergProfileSecuritySshKeyProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergProfileSecuritySshKeyProfileById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergProfileSecuritySshKeyProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileSecuritySshKeyProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileDataSummarizationsRaw - errors', () => {
      it('should have a retrieveIcebergProfileDataSummarizationsRaw function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileDataSummarizationsRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergProfileDataSummarizationRawById - errors', () => {
      it('should have a deleteIcebergProfileDataSummarizationRawById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergProfileDataSummarizationRawById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deleteIcebergProfileDataSummarizationRawById(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergProfileDataSummarizationRawById - errors', () => {
      it('should have a retrieveIcebergProfileDataSummarizationRawById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergProfileDataSummarizationRawById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.retrieveIcebergProfileDataSummarizationRawById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergProfileDataSummarizationRawById - errors', () => {
      it('should have a createIcebergProfileDataSummarizationRawById function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergProfileDataSummarizationRawById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createIcebergProfileDataSummarizationRawById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIcebergProfileDataSummarizationRawById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergProfileDataSummarizationRawById - errors', () => {
      it('should have a updateIcebergProfileDataSummarizationRawById function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergProfileDataSummarizationRawById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.updateIcebergProfileDataSummarizationRawById(null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergProfileDataSummarizationRawById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergProfileDataSummarizationRawById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotProfileRollupSummarizationFieldProfileProfile - errors', () => {
      it('should have a retrieveHealthbotProfileRollupSummarizationFieldProfileProfile function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotProfileRollupSummarizationFieldProfileProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById - errors', () => {
      it('should have a deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById - errors', () => {
      it('should have a retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById - errors', () => {
      it('should have a createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById - errors', () => {
      it('should have a updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveSensors - errors', () => {
      it('should have a retrieveSensors function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveSensors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sensorType', (done) => {
        try {
          a.retrieveSensors(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sensorType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveSensors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById - errors', () => {
      it('should have a deleteHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabase - errors', () => {
      it('should have a retrieveHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabase function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById - errors', () => {
      it('should have a createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById - errors', () => {
      it('should have a updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotSystemTriggerAction - errors', () => {
      it('should have a deleteHealthbotSystemTriggerAction function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotSystemTriggerAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotSystemTriggerAction - errors', () => {
      it('should have a retrieveHealthbotSystemTriggerAction function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotSystemTriggerAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotSystemTriggerAction - errors', () => {
      it('should have a createHealthbotSystemTriggerAction function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotSystemTriggerAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotSystemTriggerAction(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotSystemTriggerAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotSystemTriggerAction - errors', () => {
      it('should have a updateHealthbotSystemTriggerAction function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotSystemTriggerAction === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotSystemTriggerAction(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotSystemTriggerAction', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotTopicResourceResource - errors', () => {
      it('should have a retrieveHealthbotTopicResourceResource function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotTopicResourceResource === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topicName', (done) => {
        try {
          a.retrieveHealthbotTopicResourceResource(null, null, (data, error) => {
            try {
              const displayE = 'topicName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotTopicResourceResource', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotOrganizationOrganization - errors', () => {
      it('should have a retrieveHealthbotOrganizationOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotOrganizationOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotOrganizationOrganizationById - errors', () => {
      it('should have a deleteHealthbotOrganizationOrganizationById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotOrganizationOrganizationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.deleteHealthbotOrganizationOrganizationById(null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotOrganizationOrganizationById - errors', () => {
      it('should have a retrieveHealthbotOrganizationOrganizationById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotOrganizationOrganizationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.retrieveHealthbotOrganizationOrganizationById(null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotOrganizationOrganizationById - errors', () => {
      it('should have a createHealthbotOrganizationOrganizationById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotOrganizationOrganizationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.createHealthbotOrganizationOrganizationById(null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotOrganizationOrganizationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotOrganizationOrganizationById - errors', () => {
      it('should have a updateHealthbotOrganizationOrganizationById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotOrganizationOrganizationById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.updateHealthbotOrganizationOrganizationById(null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotOrganizationOrganizationById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationOrganizationById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveServicesDeviceGroupsDeviceGroupDeviceGroup - errors', () => {
      it('should have a retrieveServicesDeviceGroupsDeviceGroupDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveServicesDeviceGroupsDeviceGroupDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName - errors', () => {
      it('should have a deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServicesDeviceGroupsDeviceGroupByDeviceGroupName - errors', () => {
      it('should have a createServicesDeviceGroupsDeviceGroupByDeviceGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.createServicesDeviceGroupsDeviceGroupByDeviceGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.createServicesDeviceGroupsDeviceGroupByDeviceGroupName(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createServicesDeviceGroupsDeviceGroupByDeviceGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveServicesNetworkGroup - errors', () => {
      it('should have a retrieveServicesNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveServicesNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesNetworkGroupByNetworkGroupName - errors', () => {
      it('should have a deleteServicesNetworkGroupByNetworkGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteServicesNetworkGroupByNetworkGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.deleteServicesNetworkGroupByNetworkGroupName(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteServicesNetworkGroupByNetworkGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createServicesNetworkGroupByNetworkGroupName - errors', () => {
      it('should have a createServicesNetworkGroupByNetworkGroupName function', (done) => {
        try {
          assert.equal(true, typeof a.createServicesNetworkGroupByNetworkGroupName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.createServicesNetworkGroupByNetworkGroupName(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createServicesNetworkGroupByNetworkGroupName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveInsightsApi - errors', () => {
      it('should have a retrieveInsightsApi function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveInsightsApi === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDataStore - errors', () => {
      it('should have a deleteDataStore function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDataStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.deleteDataStore(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDataStore - errors', () => {
      it('should have a retrieveDataStore function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDataStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.retrieveDataStore(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDataStore - errors', () => {
      it('should have a createDataStore function', (done) => {
        try {
          assert.equal(true, typeof a.createDataStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.createDataStore(null, null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createDataStore('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDataStore('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDataStore - errors', () => {
      it('should have a updateDataStore function', (done) => {
        try {
          assert.equal(true, typeof a.updateDataStore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.updateDataStore(null, null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.updateDataStore('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateDataStore('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateDataStore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDevicesFactsByGroup - errors', () => {
      it('should have a retrieveIcebergDevicesFactsByGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDevicesFactsByGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveIcebergDevicesFactsByGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergDevicesFactsByGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDeviceDeviceFactsById - errors', () => {
      it('should have a retrieveIcebergDeviceDeviceFactsById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDeviceDeviceFactsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveIcebergDeviceDeviceFactsById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergDeviceDeviceFactsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergDevicesDevicesFacts - errors', () => {
      it('should have a retrieveIcebergDevicesDevicesFacts function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergDevicesDevicesFacts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDeleteAllLicense - errors', () => {
      it('should have a deleteIcebergDeleteAllLicense function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDeleteAllLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIcebergAddLicenseFromFile - errors', () => {
      it('should have a createIcebergAddLicenseFromFile function', (done) => {
        try {
          assert.equal(true, typeof a.createIcebergAddLicenseFromFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseFile', (done) => {
        try {
          a.createIcebergAddLicenseFromFile(null, (data, error) => {
            try {
              const displayE = 'licenseFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIcebergAddLicenseFromFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergGetAllLicenseId - errors', () => {
      it('should have a retrieveIcebergGetAllLicenseId function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergGetAllLicenseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIcebergReplaceLicense - errors', () => {
      it('should have a updateIcebergReplaceLicense function', (done) => {
        try {
          assert.equal(true, typeof a.updateIcebergReplaceLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIcebergReplaceLicense(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIcebergReplaceLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIcebergDeleteLicenseById - errors', () => {
      it('should have a deleteIcebergDeleteLicenseById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIcebergDeleteLicenseById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseId', (done) => {
        try {
          a.deleteIcebergDeleteLicenseById(null, (data, error) => {
            try {
              const displayE = 'licenseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteIcebergDeleteLicenseById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergLicenseFileByLicenseId - errors', () => {
      it('should have a retrieveIcebergLicenseFileByLicenseId function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergLicenseFileByLicenseId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseId', (done) => {
        try {
          a.retrieveIcebergLicenseFileByLicenseId(null, (data, error) => {
            try {
              const displayE = 'licenseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergLicenseFileByLicenseId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergLicenseKeyContents - errors', () => {
      it('should have a retrieveIcebergLicenseKeyContents function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergLicenseKeyContents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergLicenseKeyContentsById - errors', () => {
      it('should have a retrieveIcebergLicenseKeyContentsById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergLicenseKeyContentsById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseId', (done) => {
        try {
          a.retrieveIcebergLicenseKeyContentsById(null, (data, error) => {
            try {
              const displayE = 'licenseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveIcebergLicenseKeyContentsById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveIcebergLicenseFeaturesInfo - errors', () => {
      it('should have a retrieveIcebergLicenseFeaturesInfo function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveIcebergLicenseFeaturesInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAvailableNodes - errors', () => {
      it('should have a retrieveAvailableNodes function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveAvailableNodes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveTsdbCounters - errors', () => {
      it('should have a retrieveTsdbCounters function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveTsdbCounters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryTsdb - errors', () => {
      it('should have a queryTsdb function', (done) => {
        try {
          assert.equal(true, typeof a.queryTsdb === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing db', (done) => {
        try {
          a.queryTsdb(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'db is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdb', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroup', (done) => {
        try {
          a.queryTsdb('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdb', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.queryTsdb('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdb', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#queryTsdbPost - errors', () => {
      it('should have a queryTsdbPost function', (done) => {
        try {
          assert.equal(true, typeof a.queryTsdbPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing db', (done) => {
        try {
          a.queryTsdbPost(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'db is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdbPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroup', (done) => {
        try {
          a.queryTsdbPost('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdbPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.queryTsdbPost('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-queryTsdbPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateResourceDependencies - errors', () => {
      it('should have a generateResourceDependencies function', (done) => {
        try {
          assert.equal(true, typeof a.generateResourceDependencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveSensorDeviceGroup - errors', () => {
      it('should have a retrieveSensorDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveSensorDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveSensorDeviceGroup(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveSensorDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveSystemDetails - errors', () => {
      it('should have a retrieveSystemDetails function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveSystemDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userLogin - errors', () => {
      it('should have a userLogin function', (done) => {
        try {
          assert.equal(true, typeof a.userLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.userLogin(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-userLogin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userLogout - errors', () => {
      it('should have a userLogout function', (done) => {
        try {
          assert.equal(true, typeof a.userLogout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.userLogout(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-userLogout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshToken - errors', () => {
      it('should have a refreshToken function', (done) => {
        try {
          assert.equal(true, typeof a.refreshToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.refreshToken(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-refreshToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthbotAlterAppSettings - errors', () => {
      it('should have a healthbotAlterAppSettings function', (done) => {
        try {
          assert.equal(true, typeof a.healthbotAlterAppSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotOrganizationSiteSiteById - errors', () => {
      it('should have a deleteHealthbotOrganizationSiteSiteById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotOrganizationSiteSiteById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.deleteHealthbotOrganizationSiteSiteById(null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.deleteHealthbotOrganizationSiteSiteById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotOrganizationSiteSiteById - errors', () => {
      it('should have a retrieveHealthbotOrganizationSiteSiteById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotOrganizationSiteSiteById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.retrieveHealthbotOrganizationSiteSiteById(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.retrieveHealthbotOrganizationSiteSiteById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotOrganizationSiteSiteById - errors', () => {
      it('should have a createHealthbotOrganizationSiteSiteById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotOrganizationSiteSiteById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.createHealthbotOrganizationSiteSiteById(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.createHealthbotOrganizationSiteSiteById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotOrganizationSiteSiteById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotOrganizationSiteSiteById - errors', () => {
      it('should have a updateHealthbotOrganizationSiteSiteById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotOrganizationSiteSiteById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.updateHealthbotOrganizationSiteSiteById(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.updateHealthbotOrganizationSiteSiteById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotOrganizationSiteSiteById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteSiteById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotOrganizationSiteEdgeEdgeById - errors', () => {
      it('should have a deleteHealthbotOrganizationSiteEdgeEdgeById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotOrganizationSiteEdgeEdgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.deleteHealthbotOrganizationSiteEdgeEdgeById(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.deleteHealthbotOrganizationSiteEdgeEdgeById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeName', (done) => {
        try {
          a.deleteHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'edgeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotOrganizationSiteEdgeEdgeById - errors', () => {
      it('should have a retrieveHealthbotOrganizationSiteEdgeEdgeById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotOrganizationSiteEdgeEdgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.retrieveHealthbotOrganizationSiteEdgeEdgeById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.retrieveHealthbotOrganizationSiteEdgeEdgeById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeName', (done) => {
        try {
          a.retrieveHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotOrganizationSiteEdgeEdgeById - errors', () => {
      it('should have a createHealthbotOrganizationSiteEdgeEdgeById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotOrganizationSiteEdgeEdgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.createHealthbotOrganizationSiteEdgeEdgeById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.createHealthbotOrganizationSiteEdgeEdgeById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeName', (done) => {
        try {
          a.createHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotOrganizationSiteEdgeEdgeById - errors', () => {
      it('should have a updateHealthbotOrganizationSiteEdgeEdgeById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotOrganizationSiteEdgeEdgeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationName', (done) => {
        try {
          a.updateHealthbotOrganizationSiteEdgeEdgeById(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.updateHealthbotOrganizationSiteEdgeEdgeById('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing edgeName', (done) => {
        try {
          a.updateHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'edgeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotOrganizationSiteEdgeEdgeById('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotOrganizationSiteEdgeEdgeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowWorkflow - errors', () => {
      it('should have a retrieveHealthbotWorkflowWorkflow function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowWorkflow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotWorkflowWorkflowById - errors', () => {
      it('should have a deleteHealthbotWorkflowWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotWorkflowWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.deleteHealthbotWorkflowWorkflowById(null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowWorkflowById - errors', () => {
      it('should have a retrieveHealthbotWorkflowWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.retrieveHealthbotWorkflowWorkflowById(null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotWorkflowWorkflowById - errors', () => {
      it('should have a createHealthbotWorkflowWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotWorkflowWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.createHealthbotWorkflowWorkflowById(null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotWorkflowWorkflowById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotWorkflowWorkflowById - errors', () => {
      it('should have a updateHealthbotWorkflowWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotWorkflowWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.updateHealthbotWorkflowWorkflowById(null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotWorkflowWorkflowById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotWorkflowsWorkflowById - errors', () => {
      it('should have a deleteHealthbotWorkflowsWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotWorkflowsWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowsWorkflowById - errors', () => {
      it('should have a retrieveHealthbotWorkflowsWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowsWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotWorkflowsWorkflowById - errors', () => {
      it('should have a createHealthbotWorkflowsWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotWorkflowsWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotWorkflowsWorkflowById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotWorkflowsWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotWorkflowsWorkflowById - errors', () => {
      it('should have a updateHealthbotWorkflowsWorkflowById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotWorkflowsWorkflowById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotWorkflowsWorkflowById(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowsWorkflowById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveInstancesScheduleState - errors', () => {
      it('should have a retrieveInstancesScheduleState function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveInstancesScheduleState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.retrieveInstancesScheduleState(null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveInstancesScheduleState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupType', (done) => {
        try {
          a.retrieveInstancesScheduleState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveInstancesScheduleState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInstancesScheduleState - errors', () => {
      it('should have a updateInstancesScheduleState function', (done) => {
        try {
          assert.equal(true, typeof a.updateInstancesScheduleState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.updateInstancesScheduleState(null, null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateInstancesScheduleState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupType', (done) => {
        try {
          a.updateInstancesScheduleState('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateInstancesScheduleState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateInstancesScheduleState('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateInstancesScheduleState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveLogsForDeviceGroup - errors', () => {
      it('should have a retrieveLogsForDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveLogsForDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveLogsForDeviceGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveLogsForDeviceGroupService - errors', () => {
      it('should have a retrieveLogsForDeviceGroupService function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveLogsForDeviceGroupService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveLogsForDeviceGroupService(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForDeviceGroupService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.retrieveLogsForDeviceGroupService('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForDeviceGroupService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveLogsForNetworkGroup - errors', () => {
      it('should have a retrieveLogsForNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveLogsForNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveLogsForNetworkGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveLogsForNetworkGroupService - errors', () => {
      it('should have a retrieveLogsForNetworkGroupService function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveLogsForNetworkGroupService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveLogsForNetworkGroupService(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForNetworkGroupService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.retrieveLogsForNetworkGroupService('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveLogsForNetworkGroupService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDebugForScenario - errors', () => {
      it('should have a retrieveDebugForScenario function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDebugForScenario === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scenarioName', (done) => {
        try {
          a.retrieveDebugForScenario(null, null, (data, error) => {
            try {
              const displayE = 'scenarioName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDebugForScenario', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthbotDebugGenerateConfiguration - errors', () => {
      it('should have a healthbotDebugGenerateConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.healthbotDebugGenerateConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDebugJobs - errors', () => {
      it('should have a retrieveDebugJobs function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDebugJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotWorkflowInstanceById - errors', () => {
      it('should have a deleteHealthbotWorkflowInstanceById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotWorkflowInstanceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.deleteHealthbotWorkflowInstanceById(null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotWorkflowInstanceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowInstanceById - errors', () => {
      it('should have a retrieveHealthbotWorkflowInstanceById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowInstanceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.retrieveHealthbotWorkflowInstanceById(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotWorkflowInstanceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotWorkflowInstanceById - errors', () => {
      it('should have a updateHealthbotWorkflowInstanceById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotWorkflowInstanceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.updateHealthbotWorkflowInstanceById(null, null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowInstanceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.updateHealthbotWorkflowInstanceById('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowInstanceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotWorkflowInstanceById - errors', () => {
      it('should have a createHealthbotWorkflowInstanceById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotWorkflowInstanceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing workflowName', (done) => {
        try {
          a.createHealthbotWorkflowInstanceById(null, null, (data, error) => {
            try {
              const displayE = 'workflowName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotWorkflowInstanceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotWorkflowInstances - errors', () => {
      it('should have a deleteHealthbotWorkflowInstances function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotWorkflowInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowInstances - errors', () => {
      it('should have a retrieveHealthbotWorkflowInstances function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotWorkflowInstances - errors', () => {
      it('should have a updateHealthbotWorkflowInstances function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotWorkflowInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.updateHealthbotWorkflowInstances(null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotWorkflowInstances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotWorkflowStatistics - errors', () => {
      it('should have a retrieveHealthbotWorkflowStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotWorkflowStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDataDatabaseTable - errors', () => {
      it('should have a retrieveDataDatabaseTable function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDataDatabaseTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDataDatabaseTableColumnByTableName - errors', () => {
      it('should have a retrieveDataDatabaseTableColumnByTableName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDataDatabaseTableColumnByTableName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tableName', (done) => {
        try {
          a.retrieveDataDatabaseTableColumnByTableName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'tableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDataDatabaseTableColumnByTableName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDataDatabaseTagsByTableName - errors', () => {
      it('should have a retrieveDataDatabaseTagsByTableName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDataDatabaseTagsByTableName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tableName', (done) => {
        try {
          a.retrieveDataDatabaseTagsByTableName(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDataDatabaseTagsByTableName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotDeviceDetailsByUuids - errors', () => {
      it('should have a retrieveHealthbotDeviceDetailsByUuids function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotDeviceDetailsByUuids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.retrieveHealthbotDeviceDetailsByUuids(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotDeviceDetailsByUuids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEvents - errors', () => {
      it('should have a retrieveEvents function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEvents(null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEvent - errors', () => {
      it('should have a retrieveEvent function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEvent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEvent(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveEvent('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEvent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEventDeviceGroup - errors', () => {
      it('should have a retrieveEventDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEventDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEventDeviceGroup(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveEventDeviceGroup('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEventByEventNameDeviceGroup - errors', () => {
      it('should have a retrieveEventByEventNameDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEventByEventNameDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventName', (done) => {
        try {
          a.retrieveEventByEventNameDeviceGroup(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'eventName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEventByEventNameDeviceGroup('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveEventByEventNameDeviceGroup('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEventNetworkGroup - errors', () => {
      it('should have a retrieveEventNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEventNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEventNetworkGroup(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveEventNetworkGroup('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEventByEventNameNetworkGroup - errors', () => {
      it('should have a retrieveEventByEventNameNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEventByEventNameNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventName', (done) => {
        try {
          a.retrieveEventByEventNameNetworkGroup(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'eventName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEventByEventNameNetworkGroup('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveEventByEventNameNetworkGroup('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventNameNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveEventByEventName - errors', () => {
      it('should have a retrieveEventByEventName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveEventByEventName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventName', (done) => {
        try {
          a.retrieveEventByEventName(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'eventName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromTimestamp', (done) => {
        try {
          a.retrieveEventByEventName('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromTimestamp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveEventByEventName('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveEventByEventName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFieldsFromXpath - errors', () => {
      it('should have a getFieldsFromXpath function', (done) => {
        try {
          assert.equal(true, typeof a.getFieldsFromXpath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing xpath', (done) => {
        try {
          a.getFieldsFromXpath(null, null, (data, error) => {
            try {
              const displayE = 'xpath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-getFieldsFromXpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreGrafana - errors', () => {
      it('should have a restoreGrafana function', (done) => {
        try {
          assert.equal(true, typeof a.restoreGrafana === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing restoreFile', (done) => {
        try {
          a.restoreGrafana(null, (data, error) => {
            try {
              const displayE = 'restoreFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-restoreGrafana', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupGrafana - errors', () => {
      it('should have a backupGrafana function', (done) => {
        try {
          assert.equal(true, typeof a.backupGrafana === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGrafanaLogin - errors', () => {
      it('should have a getGrafanaLogin function', (done) => {
        try {
          assert.equal(true, typeof a.getGrafanaLogin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthTreeByDeviceGroup - errors', () => {
      it('should have a retrieveHealthTreeByDeviceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthTreeByDeviceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.retrieveHealthTreeByDeviceGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthTreeByDeviceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthTreeByNetworkGroup - errors', () => {
      it('should have a retrieveHealthTreeByNetworkGroup function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthTreeByNetworkGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.retrieveHealthTreeByNetworkGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthTreeByNetworkGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthTreeById - errors', () => {
      it('should have a retrieveHealthTreeById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthTreeById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveHealthTreeById(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthTreeById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthAll - errors', () => {
      it('should have a retrieveHealthAll function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInspectCommandRpcTable - errors', () => {
      it('should have a postInspectCommandRpcTable function', (done) => {
        try {
          assert.equal(true, typeof a.postInspectCommandRpcTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postInspectCommandRpcTable(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-postInspectCommandRpcTable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveDeviceTriggerInfo - errors', () => {
      it('should have a retrieveDeviceTriggerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveDeviceTriggerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.retrieveDeviceTriggerInfo(null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveDeviceTriggerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIcebergDevicesFromGroup - errors', () => {
      it('should have a removeIcebergDevicesFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeIcebergDevicesFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceGroupName', (done) => {
        try {
          a.removeIcebergDevicesFromGroup(null, (data, error) => {
            try {
              const displayE = 'deviceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-removeIcebergDevicesFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeIcebergNetworkGroupNetworkGroupById - errors', () => {
      it('should have a removeIcebergNetworkGroupNetworkGroupById function', (done) => {
        try {
          assert.equal(true, typeof a.removeIcebergNetworkGroupNetworkGroupById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkGroupName', (done) => {
        try {
          a.removeIcebergNetworkGroupNetworkGroupById(null, (data, error) => {
            try {
              const displayE = 'networkGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-removeIcebergNetworkGroupNetworkGroupById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestSettingsPaa - errors', () => {
      it('should have a retrieveHealthbotIngestSettingsPaa function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestSettingsPaa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestPaaByPaaName - errors', () => {
      it('should have a deleteHealthbotIngestPaaByPaaName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestPaaByPaaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paaName', (done) => {
        try {
          a.deleteHealthbotIngestPaaByPaaName(null, (data, error) => {
            try {
              const displayE = 'paaName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestPaaByPaaName - errors', () => {
      it('should have a retrieveHealthbotIngestPaaByPaaName function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestPaaByPaaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paaName', (done) => {
        try {
          a.retrieveHealthbotIngestPaaByPaaName(null, null, (data, error) => {
            try {
              const displayE = 'paaName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateIngestPaaByPaaName - errors', () => {
      it('should have a updateIngestPaaByPaaName function', (done) => {
        try {
          assert.equal(true, typeof a.updateIngestPaaByPaaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paaName', (done) => {
        try {
          a.updateIngestPaaByPaaName(null, null, (data, error) => {
            try {
              const displayE = 'paaName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateIngestPaaByPaaName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createIngestPaaByPaaName - errors', () => {
      it('should have a createIngestPaaByPaaName function', (done) => {
        try {
          assert.equal(true, typeof a.createIngestPaaByPaaName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paaName', (done) => {
        try {
          a.createIngestPaaByPaaName(null, null, (data, error) => {
            try {
              const displayE = 'paaName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createIngestPaaByPaaName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createIngestPaaByPaaName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestIfa - errors', () => {
      it('should have a deleteHealthbotIngestIfa function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestIfa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestIfa - errors', () => {
      it('should have a retrieveHealthbotIngestIfa function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestIfa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestIfa - errors', () => {
      it('should have a createHealthbotIngestIfa function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestIfa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestIfa(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestIfa', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestIfa - errors', () => {
      it('should have a updateHealthbotIngestIfa function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestIfa === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestIfa(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestIfa', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestIfaDeviceIds - errors', () => {
      it('should have a retrieveHealthbotIngestIfaDeviceIds function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestIfaDeviceIds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestIfaDevices - errors', () => {
      it('should have a retrieveHealthbotIngestIfaDevices function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestIfaDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHealthbotIngestIfaDeviceById - errors', () => {
      it('should have a deleteHealthbotIngestIfaDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHealthbotIngestIfaDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteHealthbotIngestIfaDeviceById(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-deleteHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveHealthbotIngestIfaDeviceById - errors', () => {
      it('should have a retrieveHealthbotIngestIfaDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveHealthbotIngestIfaDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.retrieveHealthbotIngestIfaDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-retrieveHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHealthbotIngestIfaDeviceById - errors', () => {
      it('should have a createHealthbotIngestIfaDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.createHealthbotIngestIfaDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.createHealthbotIngestIfaDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createHealthbotIngestIfaDeviceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-createHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHealthbotIngestIfaDeviceById - errors', () => {
      it('should have a updateHealthbotIngestIfaDeviceById function', (done) => {
        try {
          assert.equal(true, typeof a.updateHealthbotIngestIfaDeviceById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateHealthbotIngestIfaDeviceById(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHealthbotIngestIfaDeviceById('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-updateHealthbotIngestIfaDeviceById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJunosEncode - errors', () => {
      it('should have a postJunosEncode function', (done) => {
        try {
          assert.equal(true, typeof a.postJunosEncode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postJunosEncode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-postJunosEncode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJunosDecode - errors', () => {
      it('should have a postJunosDecode function', (done) => {
        try {
          assert.equal(true, typeof a.postJunosDecode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postJunosDecode(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-paragon_insights-adapter-postJunosDecode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
