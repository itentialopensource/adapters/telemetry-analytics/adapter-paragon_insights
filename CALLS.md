## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Juniper Paragon Insights. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Juniper Paragon Insights.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Paragon_insights. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">retrieveOrchestrator(callback)</td>
    <td style="padding:15px">retrieve_orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/orchestrator/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollbackUnsavedConfiguration(emsSanity, callback)</td>
    <td style="padding:15px">rollback_unsaved_configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAffectedGroups(callback)</td>
    <td style="padding:15px">retrieve_affected_groups</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">commitUnsavedConfiguration(sync, callback)</td>
    <td style="padding:15px">commit_unsaved_configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initialize(restartGroups, reloadRules, reloadPlaybooks, reloadSyslogPatterns, reloadSyslogPatternSets, reloadFlowTemplates, reloadSflowSchema, callback)</td>
    <td style="padding:15px">initialize</td>
    <td style="padding:15px">{base_path}/{version}/config/initialize/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestByoiIngestMappings(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_byoi_ingest_mappings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsByoiIngestMappings(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_byoi_ingest_mappings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkDeviceGroupUnsavedConfiguration(deviceGroupName, callback)</td>
    <td style="padding:15px">check_device_group_unsaved_configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/check/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkNetworkGroupUnsavedConfiguration(networkGroupName, callback)</td>
    <td style="padding:15px">check_network_group_unsaved_configuration</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/check/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceGroupDeviceGroup(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device-group_device-group</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDeviceGroupDeviceGroupById(deviceGroupName, callback)</td>
    <td style="padding:15px">delete_iceberg_device-group_device-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceGroupDeviceGroupById(deviceGroupName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device-group_device-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergDeviceGroupDeviceGroupById(deviceGroupName, body, callback)</td>
    <td style="padding:15px">create_iceberg_device-group_device-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergDeviceGroupDeviceGroupById(deviceGroupName, body, callback)</td>
    <td style="padding:15px">update_iceberg_device-group_device-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDeviceGroupStatus(deviceGroupName, callback)</td>
    <td style="padding:15px">retrieve_device_group_status</td>
    <td style="padding:15px">{base_path}/{version}/device-group/{pathv1}/status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDeviceGroupTriggerInfo(deviceGroupName, callback)</td>
    <td style="padding:15px">retrieve_device_group_trigger_info</td>
    <td style="padding:15px">{base_path}/{version}/device-group/{pathv1}/trigger_info/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDeviceGroupsDeviceGroupsById(callback)</td>
    <td style="padding:15px">delete_iceberg_device-groups_device-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceGroupsDeviceGroups(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device-groups_device-groups</td>
    <td style="padding:15px">{base_path}/{version}/config/device-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergDeviceGroupsDeviceGroupsById(body, callback)</td>
    <td style="padding:15px">create_iceberg_device-groups_device-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergDeviceGroupsDeviceGroupsById(body, callback)</td>
    <td style="padding:15px">update_iceberg_device-groups_device-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceDevice(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device_device</td>
    <td style="padding:15px">{base_path}/{version}/config/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDeviceDeviceById(deviceId, callback)</td>
    <td style="padding:15px">delete_iceberg_device_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceDeviceById(deviceId, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergDeviceDeviceById(deviceId, body, callback)</td>
    <td style="padding:15px">create_iceberg_device_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergDeviceDeviceById(deviceId, body, callback)</td>
    <td style="padding:15px">update_iceberg_device_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDevicesDevicesById(callback)</td>
    <td style="padding:15px">delete_iceberg_devices_devices_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDevicesDevices(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_devices_devices</td>
    <td style="padding:15px">{base_path}/{version}/config/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergDevicesDevicesById(body, callback)</td>
    <td style="padding:15px">create_iceberg_devices_devices_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergDevicesDevicesById(body, callback)</td>
    <td style="padding:15px">update_iceberg_devices_devices_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNetworkGroupNetworkGroup(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_network-group_network-group</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergNetworkGroupNetworkGroupById(networkGroupName, callback)</td>
    <td style="padding:15px">delete_iceberg_network-group_network-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNetworkGroupNetworkGroupById(networkGroupName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_network-group_network-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergNetworkGroupNetworkGroupById(networkGroupName, body, callback)</td>
    <td style="padding:15px">create_iceberg_network-group_network-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergNetworkGroupNetworkGroupById(networkGroupName, body, callback)</td>
    <td style="padding:15px">update_iceberg_network-group_network-group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetworkGroupStatus(networkGroupName, callback)</td>
    <td style="padding:15px">retrieve_network_group_status</td>
    <td style="padding:15px">{base_path}/{version}/network-group/{pathv1}/status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetworkGroupTriggerInfo(networkGroupName, callback)</td>
    <td style="padding:15px">retrieve_network_group_trigger_info</td>
    <td style="padding:15px">{base_path}/{version}/network-group/{pathv1}/trigger_info/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergNetworkGroupsNetworkGroupsById(callback)</td>
    <td style="padding:15px">delete_iceberg_network-groups_network-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNetworkGroupsNetworkGroups(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_network-groups_network-groups</td>
    <td style="padding:15px">{base_path}/{version}/config/network-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergNetworkGroupsNetworkGroupsById(body, callback)</td>
    <td style="padding:15px">create_iceberg_network-groups_network-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergNetworkGroupsNetworkGroupsById(body, callback)</td>
    <td style="padding:15px">update_iceberg_network-groups_network-groups_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNotificationNotification(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_notification_notification</td>
    <td style="padding:15px">{base_path}/{version}/config/notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergNotificationNotificationById(notificationName, callback)</td>
    <td style="padding:15px">delete_iceberg_notification_notification_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notification/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNotificationNotificationById(notificationName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_notification_notification_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notification/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergNotificationNotificationById(notificationName, body, callback)</td>
    <td style="padding:15px">create_iceberg_notification_notification_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notification/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergNotificationNotificationById(notificationName, body, callback)</td>
    <td style="padding:15px">update_iceberg_notification_notification_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notification/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergNotificationsNotificationsById(callback)</td>
    <td style="padding:15px">delete_iceberg_notifications_notifications_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergNotificationsNotificationsById(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_notifications_notifications_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergNotificationsNotificationsById(body, callback)</td>
    <td style="padding:15px">create_iceberg_notifications_notifications_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergNotificationsNotificationsById(body, callback)</td>
    <td style="padding:15px">update_iceberg_notifications_notifications_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/notifications/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergPlaybookPlaybook(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_playbook_playbook</td>
    <td style="padding:15px">{base_path}/{version}/config/playbook/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergPlaybookPlaybookById(playbookName, callback)</td>
    <td style="padding:15px">delete_iceberg_playbook_playbook_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbook/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergPlaybookPlaybookById(playbookName, working, download, callback)</td>
    <td style="padding:15px">retrieve_iceberg_playbook_playbook_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbook/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergPlaybookPlaybookById(playbookName, body, callback)</td>
    <td style="padding:15px">create_iceberg_playbook_playbook_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbook/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergPlaybookPlaybookById(playbookName, body, callback)</td>
    <td style="padding:15px">update_iceberg_playbook_playbook_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbook/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergPlaybooksPlaybooksById(callback)</td>
    <td style="padding:15px">delete_iceberg_playbooks_playbooks_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergPlaybooksPlaybooksById(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_playbooks_playbooks_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergPlaybooksPlaybooksById(body, callback)</td>
    <td style="padding:15px">create_iceberg_playbooks_playbooks_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergPlaybooksPlaybooksById(body, callback)</td>
    <td style="padding:15px">update_iceberg_playbooks_playbooks_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/playbooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergRetentionPoliciesRetentionPoliciesById(callback)</td>
    <td style="padding:15px">delete_iceberg_retention_policies_retention_policies_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergRetentionPoliciesRetentionPoliciesById(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_retention_policies_retention_policies_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergRetentionPoliciesRetentionPoliciesById(body, callback)</td>
    <td style="padding:15px">create_iceberg_retention_policies_retention_policies_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergRetentionPoliciesRetentionPoliciesId(body, callback)</td>
    <td style="padding:15px">update_iceberg_retention_policies_retention_policies_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergRetentionPolicyRetentionPolicy(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_retention-policy_retention-policy</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergRetentionPolicyRetentionPolicyById(retentionPolicyName, callback)</td>
    <td style="padding:15px">delete_iceberg_retention-policy_retention-policy_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policy/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergRetentionPolicyRetentionPolicyById(retentionPolicyName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_retention-policy_retention-policy_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policy/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergRetentionPolicyRetentionPolicyById(retentionPolicyName, body, callback)</td>
    <td style="padding:15px">create_iceberg_retention-policy_retention-policy_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policy/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergRetentionPolicyRetentionPolicyById(retentionPolicyName, body, callback)</td>
    <td style="padding:15px">update_iceberg_retention-policy_retention-policy_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/retention-policy/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSystemById(callback)</td>
    <td style="padding:15px">delete_iceberg_system_system_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSystem(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_system</td>
    <td style="padding:15px">{base_path}/{version}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSystemById(forceTsdb, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_system_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSystemById(forceTsdb, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_system_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSchedulers(callback)</td>
    <td style="padding:15px">delete_iceberg_system_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSchedulers(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSchedulers(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSchedulers(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSchedulerById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSchedulerById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSchedulerById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSchedulerById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemDestinations(callback)</td>
    <td style="padding:15px">delete_iceberg_system_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemDestinations(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemDestinations(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemDestinations(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemDestinationById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemDestinationById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemDestinationById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemDestinationById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemReports(callback)</td>
    <td style="padding:15px">delete_iceberg_system_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemReports(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemReports(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemReports(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemReportById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemReportById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemReportById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemReportById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsSystemSettingsById(callback)</td>
    <td style="padding:15px">delete_iceberg_system-settings_system-settings_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsSystemSettings(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system-settings_system-settings</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsSystemSettingsById(forceTsdb, body, callback)</td>
    <td style="padding:15px">create_iceberg_system-settings_system-settings_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsSystemSettingsById(forceTsdb, body, callback)</td>
    <td style="padding:15px">update_iceberg_system-settings_system-settings_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsSchedulers(callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsSchedulers(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsSchedulers(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsSchedulers(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_schedulers</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/schedulers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsSchedulerById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsSchedulerById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsSchedulerById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsSchedulerById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_scheduler_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/scheduler/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsDestinations(callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsDestinations(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsDestinations(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsDestinations(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_destinations</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destinations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsDestinationById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsDestinationById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsDestinationById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsDestinationById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_destination_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/destination/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsReports(callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsReports(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsReports(body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsReports(body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_reports</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergSystemSettingsReportById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_system_settings_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergSystemSettingsReportById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_system_settings_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergSystemSettingsReportById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_system_settings_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergSystemSettingsReportById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_system_settings_report_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system-settings/report-generation/report/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergTopicTopic(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_topic_topic</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergTopicTopicById(topicName, callback)</td>
    <td style="padding:15px">delete_iceberg_topic_topic_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergTopicTopicById(topicName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_topic_topic_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergTopicTopicById(topicName, body, callback)</td>
    <td style="padding:15px">create_iceberg_topic_topic_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergTopicTopicById(topicName, body, callback)</td>
    <td style="padding:15px">update_iceberg_topic_topic_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergTopicRuleRule(topicName, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_topic_rule_rule</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergTopicRuleRuleById(topicName, ruleName, callback)</td>
    <td style="padding:15px">delete_iceberg_topic_rule_rule_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/rule/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergTopicRuleRuleById(topicName, ruleName, working, download, callback)</td>
    <td style="padding:15px">retrieve_iceberg_topic_rule_rule_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/rule/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergTopicRuleRuleById(topicName, ruleName, body, callback)</td>
    <td style="padding:15px">create_iceberg_topic_rule_rule_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/rule/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergTopicRuleRuleById(topicName, ruleName, body, callback)</td>
    <td style="padding:15px">update_iceberg_topic_rule_rule_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/rule/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotTopicResourceResourceById(topicName, resourceName, callback)</td>
    <td style="padding:15px">delete_healthbot_topic_resource_resource_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/resource/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotTopicResourceResourceById(topicName, resourceName, body, callback)</td>
    <td style="padding:15px">create_healthbot_topic_resource_resource_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/resource/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotTopicResourceResourceById(topicName, resourceName, body, callback)</td>
    <td style="padding:15px">update_healthbot_topic_resource_resource_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/resource/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergTopicsTopicsById(callback)</td>
    <td style="padding:15px">delete_iceberg_topics_topics_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergTopicsTopics(working, sort, callback)</td>
    <td style="padding:15px">retrieve_iceberg_topics_topics</td>
    <td style="padding:15px">{base_path}/{version}/config/topics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergTopicsTopicsById(body, callback)</td>
    <td style="padding:15px">create_iceberg_topics_topics_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergTopicsTopicsById(body, callback)</td>
    <td style="padding:15px">update_iceberg_topics_topics_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFirstLogin(body, callback)</td>
    <td style="padding:15px">first_login</td>
    <td style="padding:15px">{base_path}/{version}/first-login/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotOrganizationsOrganizations(callback)</td>
    <td style="padding:15px">delete_healthbot_organizations_organizations</td>
    <td style="padding:15px">{base_path}/{version}/config/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotOrganizationsOrganizations(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_organizations_organizations</td>
    <td style="padding:15px">{base_path}/{version}/config/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotOrganizationsOrganizations(body, callback)</td>
    <td style="padding:15px">create_healthbot_organizations_organizations</td>
    <td style="padding:15px">{base_path}/{version}/config/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotOrganizationsOrganizations(body, callback)</td>
    <td style="padding:15px">update_healthbot_organizations_organizations</td>
    <td style="padding:15px">{base_path}/{version}/config/organizations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiIngestMappings(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_ingest_mappings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiIngestMappings(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_ingest_mappings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mappings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotTopicResourceResourceById(topicName, resourceName, working, download, callback)</td>
    <td style="padding:15px">retrieve_healthbot_topic_resource_resource_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/resource/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngest(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngest(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngest(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngest(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestTaggingProfiles(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestTaggingProfiles(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestTaggingProfiles(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestTaggingProfiles(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestTaggingProfileById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestTaggingProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestTaggingProfileById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestTaggingProfileById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestFlow(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestFlow(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestFlow(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestFlow(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestFlowTemplateIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_flow_template_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/template/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestFlowTemplateById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestFlowTemplateById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestFlowTemplateById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestFlowTemplateById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSflow(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_sflow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSflow(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_sflow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSflow(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_sflow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSflow(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_sflow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSflowCounterRecordById(recordName, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_sflow_counter_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/counter-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSflowCounterRecordById(recordName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_sflow_counter_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/counter-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSflowCounterRecordById(recordName, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_sflow_counter_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/counter-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSflowCounterRecordById(recordName, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_sflow_counter_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/counter-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSflowFlowRecordById(recordName, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_sflow_flow_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/flow-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSflowFlowRecordById(recordName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_sflow_flow_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/flow-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSflowFlowRecordById(recordName, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_sflow_flow_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/flow-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSflowFlowRecordById(recordName, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_sflow_flow_record_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/flow-record/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSflowSampleById(sampleName, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_sflow_sample_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/sample/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSflowSampleById(sampleName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_sflow_sample_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/sample/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSflowSampleById(sampleName, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_sflow_sample_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/sample/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSflowSampleById(sampleName, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_sflow_sample_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/sample/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSflowProtocolById(protocolName, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_sflow_protocol_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/protocol/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSflowProtocolById(protocolName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_sflow_protocol_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/protocol/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSflowProtocolById(protocolName, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_sflow_protocol_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/protocol/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSflowProtocolById(protocolName, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_sflow_protocol_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/sflow/protocol/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestFrequencyProfile(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_frequency_profile</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/frequency-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestFrequencyProfileById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_frequency_profile__by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestFrequencyProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestFrequencyProfileById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestFrequencyProfileById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestNativeGpb(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_native_gpb</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/native-gpb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestNativeGpb(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_native_gpb</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/native-gpb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestNativeGpb(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_native_gpb</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/native-gpb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestNativeGpb(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_native_gpb</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/native-gpb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestOutboundSsh(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_outbound_ssh</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/outbound-ssh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestOutboundSsh(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_outbound_ssh</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/outbound-ssh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestOutboundSsh(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_outbound_ssh</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/outbound-ssh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestOutboundSsh(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_outbound_ssh</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/outbound-ssh/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSnmpNotification(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_snmp_notification</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSnmpNotification(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_snmp_notification</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSnmpNotification(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_snmp_notification</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSnmpNotification(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_snmp_notification</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSnmpNotificationV3UsmUsernames(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_snmp_notification_v3_usm_usernames</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/user/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSnmpNotificationV3UsmUsers(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_snmp_notification_v3_usm_users</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSnmpNotificationV3UsmUserById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_snmp_notification_v3_usm_user_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/user/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSnmpNotificationV3UsmUserById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_snmp_notification_v3_usm_user_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/user/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSnmpNotificationV3UsmUserById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_snmp_notification_v3_usm_user_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/user/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSnmpNotificationV3UsmUserById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_snmp_notification_v3_usm_user_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/snmp-notification/v3/usm/user/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSyslog(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslog(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSyslog(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSyslog(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatternSetIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_pattern_set_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-set/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatternSets(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_pattern_sets</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-sets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSyslogPatternSetById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatternSetById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSyslogPatternSetById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSyslogPatternSetById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatternIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_pattern_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatterns(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_patterns</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/patterns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSyslogPatternById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSyslogPatternById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSyslogPatternById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSyslogPatternById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSyslogHeaderPatternIds(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_syslog_header_pattern_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-pattern/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSyslogHeaderPatterns(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_syslog_header_patterns</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-patterns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSyslogHeaderPatternById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_syslog_header_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSyslogHeaderPatternById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_syslog_header_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSyslogHeaderPatternById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_syslog_header_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSyslogHeaderPatternById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_syslog_header_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/syslog/header-pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiCustomPlugins(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_custom_plugins</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/custom-plugins/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestByoiCustomPluginById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiCustomPluginById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestByoiCustomPluginById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestByoiCustomPluginById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiDefaultPluginTliveKafkas(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_default_plugin_tlive_kafkas</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/default-plugin/tlive-kafka-ocs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestByoiDefaultPluginTliveKafkaById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiDefaultPluginTliveKafkaById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestByoiDefaultPluginTliveKafkaById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestByoiDefaultPluginTliveKafkaById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestByoiIngestMappingById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestByoiIngestMappingById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestByoiIngestMappingById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestByoiIngestMappingById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettings(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettings(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettings(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettings(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsTaggingProfiles(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsTaggingProfiles(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsTaggingProfiles(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsTaggingProfiles(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_tagging_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsTaggingProfileById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsTaggingProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsTaggingProfileById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsTaggingProfileById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_tagging_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/data-enrichment/tagging-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettingsFlow(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsFlow(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettingsFlow(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettingsFlow(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings_flow</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsFlowTemplateIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_flow_template_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/template/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettingsFlowTemplateById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsFlowTemplateById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettingsFlowTemplateById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettingsFlowTemplateById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings_flow_template_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/flow/template/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsFrequencyProfile(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_frequency_profile</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/frequency-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsFrequencyProfileById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_frequency_profile__by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsFrequencyProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsFrequencyProfileById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsFrequencyProfileById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_frequency_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/frequency-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettingsSyslog(callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslog(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettingsSyslog(body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettingsSyslog(body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings_syslog</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatternSetIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_pattern_set_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-set/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatternSets(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_pattern_sets</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-sets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettingsSyslogPatternSetById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatternSetById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettingsSyslogPatternSetById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettingsSyslogPatternSetById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings_syslog_pattern_set_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern-set/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatternIds(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_pattern_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatterns(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_patterns</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/patterns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergIngestSettingsSyslogPatternById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_ingest_settings_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergIngestSettingsSyslogPatternById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_ingest_settings_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergIngestSettingsSyslogPatternById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_ingest_settings_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergIngestSettingsSyslogPatternById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_ingest_settings_syslog_pattern_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/syslog/pattern/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiCustomPlugins(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_custom_plugins</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/custom-plugins/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsByoiCustomPluginById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiCustomPluginById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsByoiCustomPluginById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsByoiCustomPluginById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_byoi_custom_plugin_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/custom-plugin/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkas(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_default_plugin_tlive_kafkas</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/default-plugin/tlive-kafka-ocs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsByoiDefaultPluginTliveKafkaById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_byoi_default_plugin_tlive_kafka_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/default-plugin/tlive-kafka-oc/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestSettingsByoiIngestMappingById(name, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_settings_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsByoiIngestMappingById(name, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestSettingsByoiIngestMappingById(name, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_settings_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestSettingsByoiIngestMappingById(name, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_settings_byoi_ingest_mapping_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest-settings/byoi/ingest-mapping/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotDeploymentDeploymentById(callback)</td>
    <td style="padding:15px">delete_healthbot_deployment_deployment_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/deployment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotDeploymentDeployment(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_deployment_deployment</td>
    <td style="padding:15px">{base_path}/{version}/config/deployment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotDeploymentDeploymentById(body, callback)</td>
    <td style="padding:15px">create_healthbot_deployment_deployment_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/deployment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotDeploymentDeploymentById(body, callback)</td>
    <td style="padding:15px">update_healthbot_deployment_deployment_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/deployment/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveConfigurationJobs(jobId, jobStatus, callback)</td>
    <td style="padding:15px">retrieve_configuration_jobs</td>
    <td style="padding:15px">{base_path}/{version}/config/configuration/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveFilesHelperFiles(inputPath, callback)</td>
    <td style="padding:15px">retrieve_files_helper_files</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilesHelperFilesByFileName(fileName, inputPath, callback)</td>
    <td style="padding:15px">delete_files_helper_files_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveFilesHelperFilesByFileName(fileName, inputPath, callback)</td>
    <td style="padding:15px">retrieve_files_helper_files_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFilesHelperFilesByFileName(fileName, upFile, callback)</td>
    <td style="padding:15px">create_files_helper_files_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupHelperFiles(callback)</td>
    <td style="padding:15px">backup_helper_files</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/backup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreHelperFiles(restoreFile, callback)</td>
    <td style="padding:15px">restore_helper_files</td>
    <td style="padding:15px">{base_path}/{version}/config/files/helper-files/backup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFilesCertificatesByFileName(fileName, inputPath, certificateType, callback)</td>
    <td style="padding:15px">delete_files_certificates_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/certificates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveFilesCertificatesByFileName(fileName, inputPath, certificateType, callback)</td>
    <td style="padding:15px">retrieve_files_certificates_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/certificates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFilesCertificatesByFileName(fileName, password, certificateType, upFile, callback)</td>
    <td style="padding:15px">create_files_certificates_by_file_name</td>
    <td style="padding:15px">{base_path}/{version}/config/files/certificates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotDynamicTagging(callback)</td>
    <td style="padding:15px">retrieve_healthbot_dynamic_tagging</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotDynamicTagging(callback)</td>
    <td style="padding:15px">delete_healthbot_dynamic_tagging</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotDynamicTagging(body, callback)</td>
    <td style="padding:15px">create_healthbot_dynamic_tagging</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotDynamicTagging(body, callback)</td>
    <td style="padding:15px">update_healthbot_dynamic_tagging</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicTaggingByKey(keyName, callback)</td>
    <td style="padding:15px">get_dynamic_tagging_by_key</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/key/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDynamicTaggingByKey(keyName, body, callback)</td>
    <td style="padding:15px">create_dynamic_tagging_by_key</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/key/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicTaggingByKey(keyName, callback)</td>
    <td style="padding:15px">delete_dynamic_tagging_by_key</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/key/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDynamicTaggingByKey(keyName, body, callback)</td>
    <td style="padding:15px">update_dynamic_tagging_by_key</td>
    <td style="padding:15px">{base_path}/{version}/config/dynamic-tagging/key/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergProfiles(callback)</td>
    <td style="padding:15px">delete_iceberg_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfiles(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergProfiles(body, callback)</td>
    <td style="padding:15px">create_iceberg_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergProfiles(body, callback)</td>
    <td style="padding:15px">update_iceberg_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecurityCaProfiles(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_ca_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ca-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergProfileSecurityCaProfileById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_profile_security_ca_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ca-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecurityCaProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_ca_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ca-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergProfileSecurityCaProfileById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_profile_security_ca_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ca-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergProfileSecurityCaProfileById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_profile_security_ca_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ca-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecurityLocalCertificates(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_local_certificates</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/local-certificates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergProfileSecurityLocalCertificateById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_profile_security_local_certificate_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/local-certificate/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecurityLocalCertificateById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_local_certificate_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/local-certificate/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergProfileSecurityLocalCertificateById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_profile_security_local_certificate_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/local-certificate/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergProfileSecurityLocalCertificateById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_profile_security_local_certificate_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/local-certificate/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecuritySshKeyProfiles(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_ssh_key_profiles</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ssh-key-profiles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergProfileSecuritySshKeyProfileById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_profile_security_ssh_key_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ssh-key-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileSecuritySshKeyProfileById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_security_ssh_key_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ssh-key-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergProfileSecuritySshKeyProfileById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_profile_security_ssh_key_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ssh-key-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergProfileSecuritySshKeyProfileById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_profile_security_ssh_key_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/security/ssh-key-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileDataSummarizationsRaw(working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_data_summarizations_raw</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/data-summarizations/raw/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergProfileDataSummarizationRawById(name, callback)</td>
    <td style="padding:15px">delete_iceberg_profile_data_summarization_raw_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/data-summarization/raw/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergProfileDataSummarizationRawById(name, working, callback)</td>
    <td style="padding:15px">retrieve_iceberg_profile_data_summarization_raw_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/data-summarization/raw/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergProfileDataSummarizationRawById(name, body, callback)</td>
    <td style="padding:15px">create_iceberg_profile_data_summarization_raw_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/data-summarization/raw/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergProfileDataSummarizationRawById(name, body, callback)</td>
    <td style="padding:15px">update_iceberg_profile_data_summarization_raw_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/data-summarization/raw/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotProfileRollupSummarizationFieldProfileProfile(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_profile_rollup_summarization_field_profile_profile</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/rollup-summarization/field-profile/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(profileId, callback)</td>
    <td style="padding:15px">delete_healthbot_profile_rollup_summarization_field_profile_field_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/rollup-summarization/field-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(profileId, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_profile_rollup_summarization_field_profile_field_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/rollup-summarization/field-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(profileId, body, callback)</td>
    <td style="padding:15px">create_healthbot_profile_rollup_summarization_field_profile_field_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/rollup-summarization/field-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotProfileRollupSummarizationFieldProfileFieldProfileById(profileId, body, callback)</td>
    <td style="padding:15px">update_healthbot_profile_rollup_summarization_field_profile_field_profile_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/profile/rollup-summarization/field-profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveSensors(sensorType, sensorName, depth, append, snmpTable, callback)</td>
    <td style="padding:15px">retrieve_sensors</td>
    <td style="padding:15px">{base_path}/{version}/config/sensors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById(callback)</td>
    <td style="padding:15px">delete_healthbot_system_time-series-database_time-series-database_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/tsdb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabase(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_system_time-series-database_time-series-database</td>
    <td style="padding:15px">{base_path}/{version}/config/system/tsdb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById(forceTsdb, body, callback)</td>
    <td style="padding:15px">create_healthbot_system_time-series-database_time-series-database_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/tsdb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotSystemTimeSeriesDatabaseTimeSeriesDatabaseById(forceTsdb, body, callback)</td>
    <td style="padding:15px">update_healthbot_system_time-series-database_time-series-database_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/system/tsdb/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotSystemTriggerAction(callback)</td>
    <td style="padding:15px">delete_healthbot_system_trigger_action</td>
    <td style="padding:15px">{base_path}/{version}/config/system/trigger_action/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotSystemTriggerAction(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_system_trigger_action</td>
    <td style="padding:15px">{base_path}/{version}/config/system/trigger_action/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotSystemTriggerAction(body, callback)</td>
    <td style="padding:15px">create_healthbot_system_trigger_action</td>
    <td style="padding:15px">{base_path}/{version}/config/system/trigger_action/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotSystemTriggerAction(body, callback)</td>
    <td style="padding:15px">update_healthbot_system_trigger_action</td>
    <td style="padding:15px">{base_path}/{version}/config/system/trigger_action/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotTopicResourceResource(topicName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_topic_resource_resource</td>
    <td style="padding:15px">{base_path}/{version}/config/topic/{pathv1}/resource/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotOrganizationOrganization(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_organization_organization</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotOrganizationOrganizationById(organizationName, callback)</td>
    <td style="padding:15px">delete_healthbot_organization_organization_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotOrganizationOrganizationById(organizationName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_organization_organization_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotOrganizationOrganizationById(organizationName, body, callback)</td>
    <td style="padding:15px">create_healthbot_organization_organization_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotOrganizationOrganizationById(organizationName, body, callback)</td>
    <td style="padding:15px">update_healthbot_organization_organization_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveServicesDeviceGroupsDeviceGroupDeviceGroup(callback)</td>
    <td style="padding:15px">retrieve_services_device-groups_device-group_device-group</td>
    <td style="padding:15px">{base_path}/{version}/config/services/device-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesDeviceGroupsDeviceGroupByDeviceGroupName(deviceGroupName, callback)</td>
    <td style="padding:15px">delete_services_device-groups_device-group_by_device_group_name</td>
    <td style="padding:15px">{base_path}/{version}/config/services/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServicesDeviceGroupsDeviceGroupByDeviceGroupName(deviceGroupName, callback)</td>
    <td style="padding:15px">create_services_device-groups_device-group_by_device_group_name</td>
    <td style="padding:15px">{base_path}/{version}/config/services/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveServicesNetworkGroup(callback)</td>
    <td style="padding:15px">retrieve_services_network-group</td>
    <td style="padding:15px">{base_path}/{version}/config/services/network-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesNetworkGroupByNetworkGroupName(networkGroupName, callback)</td>
    <td style="padding:15px">delete_services_network-group_by_network_group_name</td>
    <td style="padding:15px">{base_path}/{version}/config/services/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServicesNetworkGroupByNetworkGroupName(networkGroupName, callback)</td>
    <td style="padding:15px">create_services_network-group_by_network_group_name</td>
    <td style="padding:15px">{base_path}/{version}/config/services/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(callback)</td>
    <td style="padding:15px">retrieve_defined_api</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveInsightsApi(callback)</td>
    <td style="padding:15px">retrieve_insights_api</td>
    <td style="padding:15px">{base_path}/{version}/insights/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataStore(groupName, key, callback)</td>
    <td style="padding:15px">delete_data_store</td>
    <td style="padding:15px">{base_path}/{version}/config/data-store/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDataStore(groupName, key, callback)</td>
    <td style="padding:15px">retrieve_data_store</td>
    <td style="padding:15px">{base_path}/{version}/config/data-store/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDataStore(key, groupName, body, callback)</td>
    <td style="padding:15px">create_data_store</td>
    <td style="padding:15px">{base_path}/{version}/config/data-store/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDataStore(key, groupName, body, callback)</td>
    <td style="padding:15px">update_data_store</td>
    <td style="padding:15px">{base_path}/{version}/config/data-store/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDevicesFactsByGroup(deviceGroupName, working, update, timeout, callback)</td>
    <td style="padding:15px">retrieve_iceberg_devices_facts_by_group</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/facts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDeviceDeviceFactsById(deviceId, working, update, timeout, callback)</td>
    <td style="padding:15px">retrieve_iceberg_device_device_facts_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/device/{pathv1}/facts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergDevicesDevicesFacts(working, update, timeout, callback)</td>
    <td style="padding:15px">retrieve_iceberg_devices_devices_facts</td>
    <td style="padding:15px">{base_path}/{version}/config/devices/facts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDeleteAllLicense(callback)</td>
    <td style="padding:15px">delete_iceberg_delete_all_license</td>
    <td style="padding:15px">{base_path}/{version}/license/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIcebergAddLicenseFromFile(licenseFile, callback)</td>
    <td style="padding:15px">create_iceberg_add_license_from_file</td>
    <td style="padding:15px">{base_path}/{version}/license/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergGetAllLicenseId(callback)</td>
    <td style="padding:15px">retrieve_iceberg_get_all_license_id</td>
    <td style="padding:15px">{base_path}/{version}/license/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIcebergReplaceLicense(body, callback)</td>
    <td style="padding:15px">update_iceberg_replace_license</td>
    <td style="padding:15px">{base_path}/{version}/license/keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIcebergDeleteLicenseById(licenseId, callback)</td>
    <td style="padding:15px">delete_iceberg_delete_license_by_id</td>
    <td style="padding:15px">{base_path}/{version}/license/key/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergLicenseFileByLicenseId(licenseId, callback)</td>
    <td style="padding:15px">retrieve_iceberg_license_file_by_license_id</td>
    <td style="padding:15px">{base_path}/{version}/license/key/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergLicenseKeyContents(callback)</td>
    <td style="padding:15px">retrieve_iceberg_license_key_contents</td>
    <td style="padding:15px">{base_path}/{version}/license/keys/contents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergLicenseKeyContentsById(licenseId, callback)</td>
    <td style="padding:15px">retrieve_iceberg_license_key_contents_by_id</td>
    <td style="padding:15px">{base_path}/{version}/license/key/{pathv1}/contents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIcebergLicenseFeaturesInfo(callback)</td>
    <td style="padding:15px">retrieve_iceberg_license_features_info</td>
    <td style="padding:15px">{base_path}/{version}/license/status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveAvailableNodes(callback)</td>
    <td style="padding:15px">retrieve_available_nodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveTsdbCounters(callback)</td>
    <td style="padding:15px">retrieve_tsdb_counters</td>
    <td style="padding:15px">{base_path}/{version}/tsdb-counters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryTsdb(db, deviceGroup, device, measurement, topic, rule, trigger, fields, order, groupBy, limit, where, q, callback)</td>
    <td style="padding:15px">query_tsdb</td>
    <td style="padding:15px">{base_path}/{version}/tsdb/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryTsdbPost(db, deviceGroup, device, measurement, topic, rule, trigger, fields, order, groupBy, limit, where, q, body, callback)</td>
    <td style="padding:15px">query_tsdb_post</td>
    <td style="padding:15px">{base_path}/{version}/tsdb/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateResourceDependencies(callback)</td>
    <td style="padding:15px">generate_resource_dependencies</td>
    <td style="padding:15px">{base_path}/{version}/config/rca/generate-resource-dependencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveSensorDeviceGroup(deviceGroupName, callback)</td>
    <td style="padding:15px">retrieve_sensor_device_group</td>
    <td style="padding:15px">{base_path}/{version}/config/sensor/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveSystemDetails(serviceName, callback)</td>
    <td style="padding:15px">retrieve_system_details</td>
    <td style="padding:15px">{base_path}/{version}/system-details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userLogin(body, callback)</td>
    <td style="padding:15px">user_login</td>
    <td style="padding:15px">{base_path}/{version}/login/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userLogout(body, callback)</td>
    <td style="padding:15px">user_logout</td>
    <td style="padding:15px">{base_path}/{version}/logout/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshToken(body, callback)</td>
    <td style="padding:15px">refresh_token</td>
    <td style="padding:15px">{base_path}/{version}/token/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthbotAlterAppSettings(body, callback)</td>
    <td style="padding:15px">healthbot_alter_app_settings</td>
    <td style="padding:15px">{base_path}/{version}/config/app-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotOrganizationSiteSiteById(organizationName, siteName, callback)</td>
    <td style="padding:15px">delete_healthbot_organization_site_site_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotOrganizationSiteSiteById(organizationName, siteName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_organization_site_site_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotOrganizationSiteSiteById(organizationName, siteName, body, callback)</td>
    <td style="padding:15px">create_healthbot_organization_site_site_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotOrganizationSiteSiteById(organizationName, siteName, body, callback)</td>
    <td style="padding:15px">update_healthbot_organization_site_site_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotOrganizationSiteEdgeEdgeById(organizationName, siteName, edgeName, callback)</td>
    <td style="padding:15px">delete_healthbot_organization_site_edge_edge_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/edge/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotOrganizationSiteEdgeEdgeById(organizationName, siteName, edgeName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_organization_site_edge_edge_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/edge/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotOrganizationSiteEdgeEdgeById(organizationName, siteName, edgeName, body, callback)</td>
    <td style="padding:15px">create_healthbot_organization_site_edge_edge_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/edge/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotOrganizationSiteEdgeEdgeById(organizationName, siteName, edgeName, body, callback)</td>
    <td style="padding:15px">update_healthbot_organization_site_edge_edge_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/organization/{pathv1}/site/{pathv2}/edge/{pathv3}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowWorkflow(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflow_workflow</td>
    <td style="padding:15px">{base_path}/{version}/config/workflow/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotWorkflowWorkflowById(workflowName, callback)</td>
    <td style="padding:15px">delete_healthbot_workflow_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflow/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowWorkflowById(workflowName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflow_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflow/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotWorkflowWorkflowById(workflowName, body, callback)</td>
    <td style="padding:15px">create_healthbot_workflow_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflow/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotWorkflowWorkflowById(workflowName, body, callback)</td>
    <td style="padding:15px">update_healthbot_workflow_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflow/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotWorkflowsWorkflowById(callback)</td>
    <td style="padding:15px">delete_healthbot_workflows_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflows/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowsWorkflowById(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflows_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflows/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotWorkflowsWorkflowById(body, callback)</td>
    <td style="padding:15px">create_healthbot_workflows_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflows/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotWorkflowsWorkflowById(body, callback)</td>
    <td style="padding:15px">update_healthbot_workflows_workflow_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/workflows/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveInstancesScheduleState(groupName, groupType, callback)</td>
    <td style="padding:15px">retrieve_instances_schedule_state</td>
    <td style="padding:15px">{base_path}/{version}/config/instances-schedule-state/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInstancesScheduleState(groupName, groupType, body, callback)</td>
    <td style="padding:15px">update_instances_schedule_state</td>
    <td style="padding:15px">{base_path}/{version}/config/instances-schedule-state/{pathv1}/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveLogsForDeviceGroup(deviceGroupName, download, filename, callback)</td>
    <td style="padding:15px">retrieve_logs_for_device_group</td>
    <td style="padding:15px">{base_path}/{version}/logs/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveLogsForDeviceGroupService(deviceGroupName, serviceName, download, filename, numberOfLines, callback)</td>
    <td style="padding:15px">retrieve_logs_for_device_group_service</td>
    <td style="padding:15px">{base_path}/{version}/logs/device-group/{pathv1}/service/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveLogsForNetworkGroup(networkGroupName, download, filename, callback)</td>
    <td style="padding:15px">retrieve_logs_for_network_group</td>
    <td style="padding:15px">{base_path}/{version}/logs/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveLogsForNetworkGroupService(networkGroupName, serviceName, download, filename, numberOfLines, callback)</td>
    <td style="padding:15px">retrieve_logs_for_network_group_service</td>
    <td style="padding:15px">{base_path}/{version}/logs/network-group/{pathv1}/service/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDebugForScenario(scenarioName, body, callback)</td>
    <td style="padding:15px">retrieve_debug_for_scenario</td>
    <td style="padding:15px">{base_path}/{version}/debug/scenario/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthbotDebugGenerateConfiguration(callback)</td>
    <td style="padding:15px">healthbot_debug_generate_configuration</td>
    <td style="padding:15px">{base_path}/{version}/debug/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDebugJobs(jobId, callback)</td>
    <td style="padding:15px">retrieve_debug_jobs</td>
    <td style="padding:15px">{base_path}/{version}/debug/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotWorkflowInstanceById(workflowName, workflowInstanceName, callback)</td>
    <td style="padding:15px">delete_healthbot_workflow_instance_by_id</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowInstanceById(workflowName, workflowInstanceName, extensive, callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflow_instance_by_id</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotWorkflowInstanceById(workflowName, operation, workflowInstanceName, callback)</td>
    <td style="padding:15px">update_healthbot_workflow_instance_by_id</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotWorkflowInstanceById(workflowName, body, callback)</td>
    <td style="padding:15px">create_healthbot_workflow_instance_by_id</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotWorkflowInstances(callback)</td>
    <td style="padding:15px">delete_healthbot_workflow_instances</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowInstances(callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflow_instances</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotWorkflowInstances(operation, callback)</td>
    <td style="padding:15px">update_healthbot_workflow_instances</td>
    <td style="padding:15px">{base_path}/{version}/workflow-instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotWorkflowStatistics(callback)</td>
    <td style="padding:15px">retrieve_healthbot_workflow_statistics</td>
    <td style="padding:15px">{base_path}/{version}/workflow-statistics/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDataDatabaseTable(deviceId, deviceGroupName, networkGroupName, callback)</td>
    <td style="padding:15px">retrieve_data_database_table</td>
    <td style="padding:15px">{base_path}/{version}/data/database/table/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDataDatabaseTableColumnByTableName(tableName, deviceId, deviceGroupName, networkGroupName, callback)</td>
    <td style="padding:15px">retrieve_data_database_table_column_by_table_name</td>
    <td style="padding:15px">{base_path}/{version}/data/database/table/column/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDataDatabaseTagsByTableName(tableName, deviceId, deviceGroupName, networkGroupName, tag, whereClause, callback)</td>
    <td style="padding:15px">retrieve_data_database_tags_by_table_name</td>
    <td style="padding:15px">{base_path}/{version}/data/database/table/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotDeviceDetailsByUuids(body, callback)</td>
    <td style="padding:15px">retrieve_healthbot_device_details_by_uuids</td>
    <td style="padding:15px">{base_path}/{version}/deployed-device-details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEvents(fromTimestamp, toTimestamp, color, callback)</td>
    <td style="padding:15px">retrieve_events</td>
    <td style="padding:15px">{base_path}/{version}/events/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEvent(fromTimestamp, deviceId, toTimestamp, deviceGroupName, granularity, color, callback)</td>
    <td style="padding:15px">retrieve_event</td>
    <td style="padding:15px">{base_path}/{version}/event/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEventDeviceGroup(fromTimestamp, deviceGroupName, toTimestamp, granularity, deviceId, color, callback)</td>
    <td style="padding:15px">retrieve_event_device_group</td>
    <td style="padding:15px">{base_path}/{version}/event/device-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEventByEventNameDeviceGroup(eventName, fromTimestamp, deviceGroupName, toTimestamp, granularity, deviceId, color, callback)</td>
    <td style="padding:15px">retrieve_event_by_event_name_device_group</td>
    <td style="padding:15px">{base_path}/{version}/event/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEventNetworkGroup(fromTimestamp, networkGroupName, toTimestamp, granularity, color, callback)</td>
    <td style="padding:15px">retrieve_event_network_group</td>
    <td style="padding:15px">{base_path}/{version}/event/network-group/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEventByEventNameNetworkGroup(eventName, fromTimestamp, networkGroupName, toTimestamp, granularity, color, callback)</td>
    <td style="padding:15px">retrieve_event_by_event_name_network_group</td>
    <td style="padding:15px">{base_path}/{version}/event/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveEventByEventName(eventName, fromTimestamp, deviceId, toTimestamp, deviceGroupName, granularity, color, callback)</td>
    <td style="padding:15px">retrieve_event_by_event_name</td>
    <td style="padding:15px">{base_path}/{version}/event/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFieldsFromXpath(xpath, timestamp, callback)</td>
    <td style="padding:15px">get_fields_from_xpath</td>
    <td style="padding:15px">{base_path}/{version}/field-capture/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreGrafana(restoreFile, callback)</td>
    <td style="padding:15px">restore_grafana</td>
    <td style="padding:15px">{base_path}/{version}/grafana/restore/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupGrafana(callback)</td>
    <td style="padding:15px">backup_grafana</td>
    <td style="padding:15px">{base_path}/{version}/grafana/backup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGrafanaLogin(callback)</td>
    <td style="padding:15px">grafana_login</td>
    <td style="padding:15px">{base_path}/{version}/grafana/login/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthTreeByDeviceGroup(deviceGroupName, timestamp, tolerance, device, callback)</td>
    <td style="padding:15px">retrieve_health_tree_by_device_group</td>
    <td style="padding:15px">{base_path}/{version}/health-tree/device-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthTreeByNetworkGroup(networkGroupName, timestamp, tolerance, callback)</td>
    <td style="padding:15px">retrieve_health_tree_by_network_group</td>
    <td style="padding:15px">{base_path}/{version}/health-tree/network-group/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthTreeById(deviceId, timestamp, tolerance, callback)</td>
    <td style="padding:15px">retrieve_health_tree_by_id</td>
    <td style="padding:15px">{base_path}/{version}/health-tree/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthAll(callback)</td>
    <td style="padding:15px">retrieve_health_all</td>
    <td style="padding:15px">{base_path}/{version}/health/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInspectCommandRpcTable(body, callback)</td>
    <td style="padding:15px">inspect_command_rpc_table_on_device</td>
    <td style="padding:15px">{base_path}/{version}/inspect/command-rpc/table/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDeviceTriggerInfo(deviceId, callback)</td>
    <td style="padding:15px">retrieve_device_trigger_info</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/trigger_info/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIcebergDevicesFromGroup(deviceGroupName, callback)</td>
    <td style="padding:15px">remove_iceberg_devices_from_group</td>
    <td style="padding:15px">{base_path}/{version}/config/device-group/{pathv1}/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeIcebergNetworkGroupNetworkGroupById(networkGroupName, callback)</td>
    <td style="padding:15px">remove_iceberg_network_group_network_group_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/network-group/{pathv1}/variable/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestSettingsPaa(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_settings_paa</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/paa/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestPaaByPaaName(paaName, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_paa_by_paa_name</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/paa/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestPaaByPaaName(paaName, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_paa_by_paa_name</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/paa/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIngestPaaByPaaName(paaName, body, callback)</td>
    <td style="padding:15px">update_ingest_paa_by_paa_name</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/paa/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIngestPaaByPaaName(paaName, body, callback)</td>
    <td style="padding:15px">create_ingest_paa_by_paa_name</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/paa/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestIfa(callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_ifa</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestIfa(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_ifa</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestIfa(body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_ifa</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestIfa(body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_ifa</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestIfaDeviceIds(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_ifa_device_ids</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestIfaDevices(working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_ifa_devices</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHealthbotIngestIfaDeviceById(id, callback)</td>
    <td style="padding:15px">delete_healthbot_ingest_ifa_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveHealthbotIngestIfaDeviceById(id, working, callback)</td>
    <td style="padding:15px">retrieve_healthbot_ingest_ifa_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHealthbotIngestIfaDeviceById(id, body, callback)</td>
    <td style="padding:15px">create_healthbot_ingest_ifa_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHealthbotIngestIfaDeviceById(id, body, callback)</td>
    <td style="padding:15px">update_healthbot_ingest_ifa_device_by_id</td>
    <td style="padding:15px">{base_path}/{version}/config/ingest/ifa/device/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJunosEncode(body, callback)</td>
    <td style="padding:15px">junosencode</td>
    <td style="padding:15px">{base_path}/{version}/junos-encode/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJunosDecode(body, callback)</td>
    <td style="padding:15px">junosdecode</td>
    <td style="padding:15px">{base_path}/{version}/junos-decode/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
