# Paragon Insights

Vendor: Juniper Networks
Homepage: https://www.juniper.net/us/en.html

Product: Paragon Insights
Product Page: https://www.juniper.net/us/en/products/network-automation/paragon-insights.html

## Introduction
We classify Paragon Insights into the Service Assurance domain as Paragon Insights provides advanced analytics and insights into network performance and operations.

## Why Integrate
The Paragon Insights adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Juniper Paragon Insights. With this adapter you have the ability to perform operations such as:

- Get real-time monitoring of network performance metrics.
- Get details about network logs, health status, and outliers.
- Gather and analyze configuration and telemetry data from network devices and infrastructure components.

## Additional Product Documentation
The [API documents for Juniper Paragon Insights](https://www.juniper.net/documentation/product/us/en/paragon-insights/)
